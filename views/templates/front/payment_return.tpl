{*
* 2016 Metasysco
*
* AVISO DE LICENCIA
*
* Este archivo fuente está sujeto a la Academic Free License (AFL 3.0)
* El cual está incluido en el archivo LICENCE.txt.
* También se encuentra disponible en línea, en la siguiente URL:
* http://opensource.org/licenses/afl-3.0.php
* Si por algún motivo usted no recibió una copia de esta licencia,
* o no pudo obtenerlo a través de la URL, por favor envíe un correo a
* info@metasysco.com, y en la brevedad de lo posible se le enviará una
* copia inmediata.
*
* ADVERTENCIA
*
* No edite, modifique o altére el código de este archivo, si usted
* está tiene planeado a futuro actualizar la plataforma Prestashop 
* a una nueva versión (Aplicable para la versión de prestashop 1.6.x.x).
* Si usted desea modificar este módulo para su necesidad, por favor
* contactenos por medio del correo electrónico development@metasysco.com
* o visite nuestra página web http://www.metasysco.com para mas información.
*
* @author Carlos Moreno <carlos.moreno@metasysco.com.co>
* @copyright 2016 Metasysco S.A.S.
* @version 1.1.0
* @license http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0) 
*}

{if $paymentMethod == 'card'}
	{if $resultQuery == 'success'}
		{if $paymentState == 'approved'}
			<h1 class="page-heading">{l s='CONFIRMACIÓN DE SU ORDEN' mod='mtspayuapi'}</h1>
			<div id="dvContents">
				<img class="logo img-responsive mts-print m-auto" src="https://vencompras.com/img/vencompras-logo-1468733204.jpg" alt="VenCompras" width="176" height="55">
				<h2 class="gracias-text">{l s='Gracias por tu compra!' mod='mtspayuapi'}</h2>

				<p class="mts-print">
					{l s='Has realizado una compra en nuestra tienda en linea' mod='mtspayuapi'}
				</p>
				<p class="mts-print">
					{l s='Con Tarjeta Crédito/Débito con PayU.' mod='mtspayuapi'}
				</p>

				<h3 class="mts-text-c m-top-negativo">{l s='Resumen de la transacción' mod='mtspayuapi'}</h3>

				<table border="1" id="mts_payment_result">
					<tr>
						<th class="mts_payment_result_header" colspan="2">{l s='Resumen de la operación realizada con una Tarjeta Crédito/Débito' mod='mtspayuapi'}</th>
					</tr>
					{if isset($companyName)}
					<tr>
						<td><strong>{l s='Empresa' mod='mtspayuapi'}</strong></td>
						<td>{$companyName}</td>
					</tr>
					{/if}
					{if isset($companyNit)}
					<tr>
						<td><strong>{l s='NIT' mod='mtspayuapi'}</strong></td>
						<td>{$companyNit}</td>
					</tr>
					{/if}
					{if isset($orderID)}			
					<tr>
						<td><strong>{l s='Código de orden de PayU' mod='mtspayuapi'}</strong></td>
						<td>{$orderID}</td>
					</tr>
					{/if}
					{if isset($transactionDate)}			
					<tr>
						<td><strong>{l s='Fecha' mod='mtspayuapi'}</strong></td>
						<td>{$transactionDate}</td>
					</tr>
					{/if}
					{if isset($referenceOrder)}	
					<tr>
						<td><strong>{l s='Referencia de orden' mod='mtspayuapi'}</strong></td>
						<td>{$referenceOrder}</td>
					</tr>
					{/if}
					{if isset($transactionId)}	
					<tr>
						<td><strong>{l s='Identificador de transacción' mod='mtspayuapi'}</strong></td>
						<td>{$transactionId}</td>
					</tr>
					{/if}
					{if isset($paymentNetworkResponse)}	
					<tr>
						<td><strong>{l s='Mensaje de la red de pagos' mod='mtspayuapi'}</strong></td>
						<td>{$paymentNetworkResponse}</td>
					</tr>
					{/if}
					{if isset($trazabilityCode)}	
					<tr>
						<td><strong>{l s='Código de trazabilidad' mod='mtspayuapi'}</strong></td>
						<td>{$trazabilityCode}</td>
					</tr>
					{/if}
					{if isset($errorMessage)}	
					<tr>
						<td><strong>{l s='Estado' mod='mtspayuapi'}</strong></td>
						<td>{$errorMessage}</td>
					</tr>
					{/if}
					{if isset($authorizationCode)}	
					<tr>
						<td><strong>{l s='Código de autorización' mod='mtspayuapi'}</strong></td>
						<td>{$authorizationCode}</td>
					</tr>
					{/if}
					{if isset($pendingReason)}	
					<tr>
						<td><strong>{l s='Motivo de pago pendiente' mod='mtspayuapi'}</strong></td>
						<td>{$pendingReason}</td>
					</tr>
					{/if}
					{if isset($value) && isset($currency)}	
					<tr>
						<td><strong>{l s='Valor' mod='mtspayuapi'}</strong></td>
						<td>$ {$value|number_format:0} {$currency}</td>
					</tr>
					{/if}
				</table>
			</div>	
			<div id="cart_navigation">
				<div>
					<a class="button btn btn-default standard-checkout button-small f-right payment-btn" href="/order-history">Ir a Historial de Pedidos</a>
				</div>
			</div>
		{elseif $paymentState == 'pending'}
			<h1 class="page-heading">{l s='CONFIRMACIÓN DE SU ORDEN' mod='mtspayuapi'}</h1>
			<div id="dvContents">
				<img class="logo img-responsive mts-print m-auto" src="https://vencompras.com/img/vencompras-logo-1468733204.jpg" alt="VenCompras" width="176" height="55">
				<h2 class="gracias-text">{l s='Gracias por tu compra!' mod='mtspayuapi'}</h2>

				<p class="mts-print">
					{l s='Has realizado una compra en nuestra tienda en linea' mod='mtspayuapi'}
				</p>
				<p class="mts-print">
					{l s='Con Tarjeta Crédito/Débito con PayU.' mod='mtspayuapi'}
				</p>
				<p class="mts-print">
					{l s='Estamos esperando la confirmación de tu banco para proceder con el envío.' mod='mtspayuapi'}
				</p>

				<p>
					{l s='Has realizado una compra en nuestra tienda en linea con una Tarjeta Crédito/Débito a través PayU. ' mod='mtspayuapi'}<u>{l s='Estamos esperando la confirmación de tu banco para proceder con el envío.' mod='mtspayuapi'}</u>
				</p>
				<br>

				<h3 class="mts-text-c m-top-negativo">{l s='Resumen de la transacción' mod='mtspayuapi'}</h3>

				<table border="1" id="mts_payment_result">
					<tr>
						<th class="mts_payment_result_header" colspan="2">{l s='Resumen de la operación realizada con una Tarjeta Crédito/Débito' mod='mtspayuapi'}</th>
					</tr>
					{if isset($companyName)}
					<tr>
						<td><strong>{l s='Empresa' mod='mtspayuapi'}</strong></td>
						<td>{$companyName}</td>
					</tr>
					{/if}
					{if isset($companyNit)}
					<tr>
						<td><strong>{l s='NIT' mod='mtspayuapi'}</strong></td>
						<td>{$companyNit}</td>
					</tr>
					{/if}
					{if isset($orderID)}			
					<tr>
						<td><strong>{l s='Código de orden de PayU' mod='mtspayuapi'}</strong></td>
						<td>{$orderID}</td>
					</tr>
					{/if}
					{if isset($transactionDate)}			
					<tr>
						<td><strong>{l s='Fecha' mod='mtspayuapi'}</strong></td>
						<td>{$transactionDate}</td>
					</tr>
					{/if}
					{if isset($referenceOrder)}	
					<tr>
						<td><strong>{l s='Referencia de orden' mod='mtspayuapi'}</strong></td>
						<td>{$referenceOrder}</td>
					</tr>
					{/if}
					{if isset($transactionId)}	
					<tr>
						<td><strong>{l s='Identificador de transacción' mod='mtspayuapi'}</strong></td>
						<td>{$transactionId}</td>
					</tr>
					{/if}
					{if isset($paymentNetworkResponse)}	
					<tr>
						<td><strong>{l s='Mensaje de la red de pagos' mod='mtspayuapi'}</strong></td>
						<td>{$paymentNetworkResponse}</td>
					</tr>
					{/if}
					{if isset($trazabilityCode)}	
					<tr>
						<td><strong>{l s='Código de trazabilidad' mod='mtspayuapi'}</strong></td>
						<td>{$trazabilityCode}</td>
					</tr>
					{/if}
					{if isset($errorMessage)}	
					<tr>
						<td><strong>{l s='Estado' mod='mtspayuapi'}</strong></td>
						<td>{$errorMessage}</td>
					</tr>
					{/if}
					{if isset($authorizationCode)}	
					<tr>
						<td><strong>{l s='Código de autorización' mod='mtspayuapi'}</strong></td>
						<td>{$authorizationCode}</td>
					</tr>
					{/if}
					{if isset($pendingReason)}	
					<tr>
						<td><strong>{l s='Motivo de pago pendiente' mod='mtspayuapi'}</strong></td>
						<td>{$pendingReason}</td>
					</tr>
					{/if}
					{if isset($value) && isset($currency)}	
					<tr>
						<td><strong>{l s='Valor' mod='mtspayuapi'}</strong></td>
						<td>$ {$value|number_format:0} {$currency}</td>
					</tr>
					{/if}
				</table>
			</div>	
			<div id="cart_navigation">
				<div>
					<a class="button btn btn-default standard-checkout button-small f-right payment-btn" href="/order-history">Ir a Historial de Pedidos</a>
				</div>
			</div>
		{elseif $paymentState == 'declined'}
			<h1 class="page-heading">{l s='CONFIRMACIÓN DE SU ORDEN' mod='mtspayuapi'}</h1>
			<div id="dvContents">
				<img class="logo img-responsive mts-print m-auto" src="https://vencompras.com/img/vencompras-logo-1468733204.jpg" alt="VenCompras" width="176" height="55">
				{*<h2 class="gracias-text">{l s='Algo no salió bien...' mod='mtspayuapi'}</h2>*}
                <h2 class="gracias-text">{l s='Ha ocurrido algo que no permitió procesar su orden...' mod='mtspayuapi'}</h2>

				<p class="mts-print">
					{l s='Has intentado realizar una compra en nuestra tienda en linea' mod='mtspayuapi'}
				</p>
				<p class="mts-print">
					{l s='Con Tarjeta Crédito/Débito con PayU' mod='mtspayuapi'}
				</p>
				<p class="mts-print">
					{l s='Pero algo salió mal.' mod='mtspayuapi'}
				</p>

				<p>
					{l s='Has intentado realizar una compra en nuestra tienda en linea con una Tarjeta Crédito/Débito a través PayU, pero algo salió mal y ' mod='mtspayuapi'}<u>{l s='el pago no se pudo realizar.' mod='mtspayuapi'}</u>
				</p>
				<br>

				<h3 class="mts-text-c m-top-negativo">{l s='Resumen de la transacción' mod='mtspayuapi'}</h3>

				<table border="1" id="mts_payment_result">
					<tr>
						<th class="mts_payment_result_header" colspan="2">{l s='Resumen de la operación realizada con una Tarjeta Crédito/Débito' mod='mtspayuapi'}</th>
					</tr>
					{if isset($companyName)}
					<tr>
						<td><strong>{l s='Empresa' mod='mtspayuapi'}</strong></td>
						<td>{$companyName}</td>
					</tr>
					{/if}
					{if isset($companyNit)}
					<tr>
						<td><strong>{l s='NIT' mod='mtspayuapi'}</strong></td>
						<td>{$companyNit}</td>
					</tr>
					{/if}
					{if isset($orderID)}			
					<tr>
						<td><strong>{l s='Código de orden de PayU' mod='mtspayuapi'}</strong></td>
						<td>{$orderID}</td>
					</tr>
					{/if}
					{if isset($transactionDate)}			
					<tr>
						<td><strong>{l s='Fecha' mod='mtspayuapi'}</strong></td>
						<td>{$transactionDate}</td>
					</tr>
					{/if}
					{if isset($referenceOrder)}	
					<tr>
						<td><strong>{l s='Referencia de orden' mod='mtspayuapi'}</strong></td>
						<td>{$referenceOrder}</td>
					</tr>
					{/if}
					{if isset($transactionId)}	
					<tr>
						<td><strong>{l s='Identificador de transacción' mod='mtspayuapi'}</strong></td>
						<td>{$transactionId}</td>
					</tr>
					{/if}
					{if isset($paymentNetworkResponse)}	
					<tr>
						<td><strong>{l s='Mensaje de la red de pagos' mod='mtspayuapi'}</strong></td>
						<td>{$paymentNetworkResponse}</td>
					</tr>
					{/if}
					{if isset($trazabilityCode)}	
					<tr>
						<td><strong>{l s='Código de trazabilidad' mod='mtspayuapi'}</strong></td>
						<td>{$trazabilityCode}</td>
					</tr>
					{/if}
					{if isset($errorMessage)}	
					<tr>
						<td><strong>{l s='Estado' mod='mtspayuapi'}</strong></td>
						<td>{$errorMessage}</td>
					</tr>
					{/if}
					{if isset($authorizationCode)}	
					<tr>
						<td><strong>{l s='Código de autorización' mod='mtspayuapi'}</strong></td>
						<td>{$authorizationCode}</td>
					</tr>
					{/if}
					{if isset($pendingReason)}	
					<tr>
						<td><strong>{l s='Motivo de pago pendiente' mod='mtspayuapi'}</strong></td>
						<td>{$pendingReason}</td>
					</tr>
					{/if}
					{if isset($value) && isset($currency)}	
					<tr>
						<td><strong>{l s='Valor' mod='mtspayuapi'}</strong></td>
						<td>$ {$value|number_format:0} {$currency}</td>
					</tr>
					{/if}
				</table>
			</div>	
			<div id="cart_navigation">
			{if $answerAPI != 'true'}
				<div>
					{*<a class="payment_sub" href="/order-quick?submitReorder=&id_order={$id_order}">Reintentar compra</a>*}
                    <a class="button btn btn-default standard-checkout button-small payment-btn" href="/order-quick?submitReorder=&id_order={$id_order}">Reintentar compra</a>
				</div>
			{/if}
				{*<div>*}
                <div id="cart_navigation">
					<a class="button btn btn-default standard-checkout button-small f-right payment-btn" href="/order-history">Ir a Historial de Pedidos</a>
				</div>
			</div>
		{/if}
	{elseif $resultQuery == 'error'}

	{/if}
{elseif $paymentMethod == 'pse'}
	<h1 class="page-heading">{l s='CONFIRMACIÓN DE SU ORDEN' mod='mtspayuapi'}</h1>
	<div id="dvContents">
		<img class="logo img-responsive mts-print m-auto" src="https://vencompras.com/img/vencompras-logo-1468733204.jpg" alt="VenCompras" width="176" height="55">
		<h2 class="gracias-text">{l s='Gracias por tu compra!' mod='mtspayuapi'}</h2>

		<p class="mts-print">
			{l s='Has realizado una compra en nuestra tienda en linea.' mod='mtspayuapi'}
		</p>
		<p class="mts-print">
			{l s='A través de PSE con PayU Latam.' mod='mtspayuapi'}
		</p>

		<h3 class="mts-text-c m-top-negativo">{l s='Resumen de la transacción' mod='mtspayuapi'}</h3>

		<table border="1" id="mts_payment_result">
			<tr>
				<th colspan="2">{l s='Resumen de la operación realizada en PSE' mod='mtspayuapi'}</th>
			</tr>
			<tr>
				<td><strong>{l s='Empresa' mod='mtspayuapi'}</strong></td>
				<td>{$smarty.get.merchant_name}</td>
			</tr>
			<tr>
				<td><strong>{l s='NIT' mod='mtspayuapi'}</strong></td>
				<td>{$company_nit}</td>
			</tr>
			<tr>
				<td><strong>{l s='Fecha' mod='mtspayuapi'}</strong></td>
				<td>{$transactiondate}</td>
			</tr>
			<tr>
				<td><strong>{l s='Estado' mod='mtspayuapi'}</strong></td>
				{if $smarty.get.polTransactionState == "4" || $smarty.get.polResponseCode == "1"}
					<td>{l s='Transacción aprobada' mod='mtspayuapi'}{assign "answerAPI" "true"}</td>
				{elseif $smarty.get.polTransactionState == "6" || $smarty.get.polResponseCode == "4"}
					<td>{l s='Transacción rechazada' mod='mtspayuapi'}</td>
				{elseif $smarty.get.polTransactionState == "6" || $smarty.get.polResponseCode == "5"}
					<td>{l s='Transacción fallida' mod='mtspayuapi'}</td>
				{elseif $smarty.get.polTransactionState == "12" || $smarty.get.polResponseCode == "9994"}
					<td>{l s='Transacción pendiente, por favor revisar si el débito fue realizado en el banco' mod='mtspayuapi'}</td>
				{else}
					<td>{l s='Error en transacción' mod='mtspayuapi'}</td>
				{/if}
			</tr>
			<tr>
				<td><strong>{l s='Referencia de orden' mod='mtspayuapi'}</strong></td>
				<td>{$smarty.get.referenceCode}</td>
			</tr>
			<tr>
				<td><strong>{l s='Referencia de transacción' mod='mtspayuapi'}</strong></td>
				<td>{$smarty.get.transactionId}</td>
			</tr>
			<tr>
				<td><strong>{l s='Número de transacción' mod='mtspayuapi'}</strong></td>
				<td>{$smarty.get.cus}</td>
			</tr>
			<tr>
				<td><strong>{l s='Banco' mod='mtspayuapi'}</strong></td>
				<td>{$smarty.get.pseBank}</td>
			</tr>
			<tr>
				<td><strong>{l s='Valor' mod='mtspayuapi'}</strong></td>
				<td>{$smarty.get.TX_VALUE}</td>
			</tr>
			<tr>
				<td><strong>{l s='Moneda' mod='mtspayuapi'}</strong></td>
				<td>{$smarty.get.currency}</td>
			</tr>
			<tr>
				<td><strong>{l s='Descripción' mod='mtspayuapi'}</strong></td>
				<td>{$smarty.get.description}</td>
			</tr>
			<tr>
				<td><strong>{l s='IP Origen' mod='mtspayuapi'}</strong></td>
				<td>{$smarty.get.pseReference1}</td>
			</tr>
		</table>
	</div>	
		<div id="cart_navigation">
		{if $answerAPI != 'true'}
			<div>
				{*<a class="payment_sub" href="/order-quick?submitReorder=&id_order={$id_order}">Reintentar compra</a>*}
                <a class="button btn btn-default standard-checkout button-small f-right payment-btn" href="/order-quick?submitReorder=&id_order={$id_order}">Reintentar compra</a>
			</div>
		{/if}
			{*<div>*}
            <div id="cart_navigation">
				<a class="button btn btn-default standard-checkout button-small f-right payment-btn" href="/order-history">Ir a Historial de Pedidos</a>
			</div>
		</div>

{elseif $submitMethod == "EFECTY" || $submitMethod == "BALOTO"}
	<h1 class="page-heading">{l s='CONFIRMACIÓN DE SU ORDEN' mod='mtspayuapi'}</h1>
	<div id="dvContents">
		<img class="logo img-responsive mts-print m-auto" src="https://vencompras.com/img/vencompras-logo-1468733204.jpg" alt="VenCompras" width="176" height="55">
		<h2 class="gracias-text">{l s='Gracias por tu compra!' mod='mtspayuapi'}</h2>

		<p class="mts-print">
			{l s='Has realizado una compra en nuestra tienda en linea.' mod='mtspayuapi'}
		</p>
		<p class="mts-print">
			{l s='A través de ' mod='mtspayuapi'}{$submitMethod}{l s=' con PayU Latam.' mod='mtspayuapi'}
		</p>

		<h3 class="mts-text-c m-top-negativo">{l s='Resumen de la transacción' mod='mtspayuapi'}</h3>

		<table border="1" id="mts_payment_result">
			<tr>
				<th colspan="2">{l s='Resumen de la operación realizada en ' mod='mtspayuapi'}{$submitMethod}</th>
			</tr>
			<tr>
				<td><strong>{l s='Medio de pago' mod='mtspayuapi'}</strong></td>
				<td>{$submitMethod}</td>
			</tr>
			<tr>
				<td><strong>{l s='Número de referencia de Pago' mod='mtspayuapi'}</strong></td>
				<td>{$paymentReference}</td>
			</tr>
			<tr>
				<td><strong>{l s='Total a pagar' mod='mtspayuapi'}</strong></td>
				<td>{$total} {$currency}</td>
			</tr>
			<tr>
				<td><strong>{l s='Referencia de orden' mod='mtspayuapi'}</strong></td>
				<td>{$referenceCode}</td>
			</tr>
			<tr>
				<td><strong>{l s='Plazo máximo para realizar el pago' mod='mtspayuapi'}</strong></td>
				<td>{l s='Hasta el día ' mod='mtspayuapi'}{$paymentUntilDay}{l s=' a las ' mod='mtspayuapi'}{$paymentUntilHour}</td>
			</tr>

			<tr>
				<th colspan="2" class="pay_important">
					<strong>{l s='Importante:' mod='mtspayuapi'}</strong> {l s='Recuerde que puede acercarse a cualquier establecimiento ' mod='mtspayuapi'}<strong>{$submitMethod}</strong>{l s=' con solo presentar el número de referencia de pago ' mod='mtspayuapi'}{l s='y dispone de ' mod='mtspayuapi'}{if $submitMethod == "BALOTO"}3{elseif $submitMethod == 'EFECTY'}5{/if}{l s=' días hábiles.' mod='mtspayuapi'}
				</th>
			</tr>

		</table>
		<div id="cart_navigation" class="cart_navigation m-top-negativo">
			<!-- <div>
				<a id="btnPrint" value="Print" class="payment_other"> Imprimir </a>
			</div> -->
			<div class="inlines" style="overflow: hidden;">
				<div class="pay_inline">
					<!-- hidden xs -->
					<a class="hidden-xs button btn btn-default standard-checkout button-small payment-btn" target="_blank" href="{$urlDownloadPDF}" style="float: left">Descargar comprobante</a>
					<!-- visible xs -->
					<a class="visible-xs button btn btn-default standard-checkout button-small payment-btn " target="_blank" href="{$urlDownloadPDF}" style="float: left">Comprobante</a>

				</div>
				<div class="pay_inline">
					<!-- hidden xs -->
					<a class="hidden-xs button btn btn-default standard-checkout button-small f-right payment-btn hidden-xs" href="/order-history" style="float: right">Ir a Historial de Pedidos</a>
					<!-- visible xs -->
					<a class="visible-xs button btn btn-default standard-checkout button-small f-right payment-btn" href="/order-history" style="float: right">Historial de Pedidos</a>
				</div>
			</div>
			<div class="pay_block">
				<a class="payment_sub button-exclusive btn btn-default" target="_blank" href="{$urlViewHTML}" ><i class="icon-chevron-left" style="line-height: 25px;"></i>Ver en PayU</a>
			</div>
		</div>
	</div>
{else}
	<h1 class="page-heading">{l s='¿Qué haces por aquí?' mod='mtspayuapi'}</h1>
{/if}


<!-- link js -->
<script type="application/javascript" src="{$modules_dir|escape:'html':'UTF-8'}mtspayuapi/views/templates/js/jQuery.print.js"></script> 
{literal}
<script type='text/javascript'>
	$(function () {
		$("#btnPrint").click(function () {
	        var contents = $("#dvContents").html();
	        var frame1 = $('<iframe />');
	        frame1[0].name = "frame1";
	        frame1.css({ "position": "absolute", "top": "-1000000px" });
	        $("body").append(frame1);
	        var frameDoc = frame1[0].contentWindow ? frame1[0].contentWindow : frame1[0].contentDocument.document ? frame1[0].contentDocument.document : frame1[0].contentDocument;
	        frameDoc.document.open();
	        //Create a new HTML document.
	        frameDoc.document.write('<html><head><title>DIV Contents</title>');
	        frameDoc.document.write('</head><body>');
	        //Append the external CSS file.
	        frameDoc.document.write('<link href="style.css" rel="stylesheet" type="text/css" />');
	        //Append the DIV contents.
	        frameDoc.document.write(contents);
	        frameDoc.document.write('</body></html>');
	        frameDoc.document.close();
	        setTimeout(function () {
	            window.frames["frame1"].focus();
	            window.frames["frame1"].print();
	            frame1.remove();
	        }, 500);
	    });
	});
</script>
{/literal}
<!-- Fin Link js -->