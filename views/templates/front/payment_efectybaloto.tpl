{*
* 2016 Metasysco
*
* AVISO DE LICENCIA
*
* Este archivo fuente está sujeto a la Academic Free License (AFL 3.0)
* El cual está incluido en el archivo LICENCE.txt.
* También se encuentra disponible en línea, en la siguiente URL:
* http://opensource.org/licenses/afl-3.0.php
* Si por algún motivo usted no recibió una copia de esta licencia,
* o no pudo obtenerlo a través de la URL, por favor envíe un correo a
* info@metasysco.com, y en la brevedad de lo posible se le enviará una
* copia inmediata.
*
* ADVERTENCIA
*
* No edite, modifique o altére el código de este archivo, si usted
* está tiene planeado a futuro actualizar la plataforma Prestashop 
* a una nueva versión (Aplicable para la versión de prestashop 1.6.x.x).
* Si usted desea modificar este módulo para su necesidad, por favor
* contactenos por medio del correo electrónico development@metasysco.com
* o visite nuestra página web http://www.metasysco.com para mas información.
*
* @author Carlos Moreno <carlos.moreno@metasysco.com.co>
* @copyright 2016 Metasysco S.A.S.
* @version 1.1.0
* @license http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0) 
*}

<!-- <script src="https://code.jquery.com/jquery-3.1.1.min.js" type="text/javascript" charset="utf-8" async defer></script> -->

{literal}
	<script>
		$('.mts-order').attr('id','order');
	</script>
{/literal}



{capture name=path}
	<a href="{$link->getPageLink('order', true, NULL, "step=3")|escape:'html':'UTF-8'}" title="{l s='Volver al Checkout' mod='mtspayuapi'}">{l s='Checkout' mod='mtspayuapi'}</a><span class="navigation-pipe">{$navigationPipe}</span>{l s='Pago a través de PayU' mod='mtspayuapi'}
{/capture}
<div id="module-mtspaymentapi-prevalidation">
	<h2 class="page-heading">{l s='Total de la orden' mod='mtspayuapi'}</h2>
	<div class="col col-xs-12 col-sm-12 col-md-4 col-md-push-8">
		<h4 class="elgpt m-r-negativo"> {l s='Resumen de la compra' mod='mtspayuapi'} </h4>
		<div class="box lgpt-box m-l-negativo m-r-negativo">
			<p> {l s='Productos:' mod='mtspayuapi'} </p>
			<ul class="m-l-plus">
				{foreach name=outer item=product from=$products}
					{foreach name=inner key=key item=item from=$product}
						{if $key == 'name'}
							<li style="list-style: disc;">{$item}</li>
						{/if}
					{/foreach}				
				{/foreach}
			</ul>
			<p class="payment_total">
				<span class="first">{l s='Total a pagar:' mod='mtspayuapi'}</span>
				<span class="payu-puntos"></span>
				<span class="last">$ {$total|number_format:0} {$currency}</span>
			</p>
			<p class="pay_ship">
				<strong>{l s='Envío incluido!' mod='mtspayuapi'}</strong>
			</p>
		</div>
	</div>
	<div class="col col-xs-12 col-sm-12 col-md-8 col-md-pull-4">
		<h3 class="elgpt m-r-megativo-xs">{l s='Has elegido el método de pago en línea de PayU' mod='mtspayuapi'}</h3>
		<div id="payment_pse" class="box lgpt-box m-r-megativo-xs">
			<div class="pay-ef-ba-img {$choiced_method}">
				<h4>{l s='Pago con ' mod='mtspayuapi'}{$choiced_method}</h4>
				<img class="payment_pse_pseimg" src="{$modules_dir|escape:'html':'UTF-8'}mtspayuapi/views/templates/img/icon-{$choiced_method}-2.png" alt="{$choiced_method}">
			</div>
			{if $choiced_method == 'efecty'}
				<form  id="form_efecty" class="m-top-negativo" role="form" method="post" action="{$link->getModuleLink('mtspayuapi', 'efecty_process', [], true)|escape:'html'}">
			{elseif $choiced_method == 'baloto'}
				<form  id="form_baloto" class="m-top-negativo" role="form" method="post" action="{$link->getModuleLink('mtspayuapi', 'baloto_process', [], true)|escape:'html'}">
			{/if}
				
				<div style="background:url(https://maf.pagosonline.net/ws/fp?id={$deviceSessionId})"></div>
				<img class="img-none" src="https://maf.pagosonline.net/ws/fp/clear.png?id={$deviceSessionId}">
				<script src="https://maf.pagosonline.net/ws/fp/check.js?id={$deviceSessionId}"></script>
				<object type="application/x-shockwave-flash" data="https://maf.pagosonline.net/ws/fp/fp.swf?id={$deviceSessionId}" width="1" height="1" id="thm_fp">
					<param name="movie" value="https://maf.pagosonline.net/ws/fp/fp.swf?id={$deviceSessionId}" />
				</object>
						
				<p class="m-top-negativo mts-text-c	t-bold m-bot">{l s='Para realizar el pago, debes seguir los siguientes pasos:' mod='mtspayuapi'} </p>

				<ul id="info_pay">
					<li class="row"> 
						<div class="col-xs-9 col-sm-8 info-text">
							<div>1.</div> <p>Haz click en el botón <strong>"Generar número de pago"</strong> para obtener el número que te pedirá el cajero de {$choiced_method}.</p>
						</div>
						<div class="col-xs-3 col-sm-4 info-img">
							<img src="{$modules_dir|escape:'html':'UTF-8'}mtspayuapi/views/templates/img/receipt.png" alt="">
						</div>
					</li>
					<li class="row"> 
						<div class="col-xs-9 col-sm-8 info-text">
							<div>2.</div> <p>Realiza el <strong>Pago en efectivo</strong> presentando el número que generaste, en cualquier punto {$choiced_method} de Colombia.</p>
						</div>
						<div class="col-xs-3 col-sm-4 info-img">
							<div>
								<img class="hidden-xs-mts" src="{$modules_dir|escape:'html':'UTF-8'}mtspayuapi/views/templates/img/men.png" alt="">
								<img src="{$modules_dir|escape:'html':'UTF-8'}mtspayuapi/views/templates/img/{$choiced_method}.png" alt="">
							</div>
						</div>
					</li>
					<li class="row"> 
						<div class="col-xs-9 col-sm-8 info-text">
							<div>3.</div> <p><strong>Una vez recibido tu pago en <span style="text-transform: capitalize;">{$choiced_method}</span></strong>, PayU enviará la información del pago a <strong>{$companyName}</strong>, que procederá a hacer la entrega del producto/servicio que estás adquiriendo.</p>
						</div>
						<div class="col-xs-3 col-sm-4 info-img">
							<div>
								<img src="{$modules_dir|escape:'html':'UTF-8'}mtspayuapi/views/templates/img/payment.png" alt="">
								<img class="hidden-xs-mts" src="{$modules_dir|escape:'html':'UTF-8'}mtspayuapi/views/templates/img/arrow_right.png" alt="">
								<img class="hidden-xs-mts" src="{$modules_dir|escape:'html':'UTF-8'}mtspayuapi/views/templates/img/store.png" alt="">
							</div>
						</div>
					</li>	
				</ul>
				<div class="pay-eb">
					<input type="hidden" id="choiced_method" name="choiced_method" value="{$choiced_method}">
					<input type="hidden" id="total" name="total" value="{$total}">
					<input type="hidden" id="currency" name="currency" value="{$currency}">
				</div>
				<p class="cart_navigation" id="cart_navigation">
					<button id="sendEfecty" class="button btn btn-default standard-checkout button-small f-right payment-btn">{l s='Generar número de pago' mod='mtspayuapi'}</button>
					<a href="{$link->getPageLink('order', true, NULL, "step=3")|escape:'html'}#opc_payment_methods" class="button-exclusive btn btn-default" style="line-height: 2"><i class="	icon-chevron-left" style="line-height: 2;"></i>{l s='Volver a métodos de pago' mod='mtspayuapi'}</a>
				</p>
			</form>
		</div>
	</div>
</div>