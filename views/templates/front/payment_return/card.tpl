{*
* 2016 Metasysco
*
* AVISO DE LICENCIA
*
* Este archivo fuente está sujeto a la Academic Free License (AFL 3.0)
* El cual está incluido en el archivo LICENCE.txt.
* También se encuentra disponible en línea, en la siguiente URL:
* http://opensource.org/licenses/afl-3.0.php
* Si por algún motivo usted no recibió una copia de esta licencia,
* o no pudo obtenerlo a través de la URL, por favor envíe un correo a
* info@metasysco.com, y en la brevedad de lo posible se le enviará una
* copia inmediata.
*
* ADVERTENCIA
*
* No edite, modifique o altére el código de este archivo, si usted
* está tiene planeado a futuro actualizar la plataforma Prestashop 
* a una nueva versión (Aplicable para la versión de prestashop 1.6.x.x).
* Si usted desea modificar este módulo para su necesidad, por favor
* contactenos por medio del correo electrónico development@metasysco.com
* o visite nuestra página web http://www.metasysco.com para mas información.
*
* @author Carlos Moreno <carlos.moreno@metasysco.com.co>
* @copyright 2016 Metasysco S.A.S.
* @version 1.1.0
* @license http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0) 
*}

<h1 class="page-heading">{l s='Resultado de la operación' mod='mtspayuapi'}</h1>
<div id="dvContents">

{if $resultQuery == 'success' && $paymentState == 'approved'}

	<h2 class="gracias-text">{l s='Gracias por su compra!' mod='mtspayuapi'}</h2>
	<p>
		{l s='Resumen de la operación realizada con tárjeta de crédito. ' mod='mtspayuapi'}
	</p>
	<br>

{elseif $resultQuery == 'success' && $paymentState == 'pending'}

	<h2 class="gracias-text">{l s='Gracias por tu compra!' mod='mtspayuapi'}</h2>
	<p>
		{l s='Resumen de la operación realizada con tárjeta de crédito. ' mod='mtspayuapi'}<u>{l s='Estamos esperando la confirmación de tu banco para proceder con el envío.' mod='mtspayuapi'}</u>
	</p>
	<br>

{elseif $resultQuery == 'success' && $paymentState == 'declined'}

    {*<h2 class="gracias-text">{l s='Algo no salió bien...' mod='mtspayuapi'}</h2>*}
    <h2 class="gracias-text">{l s='Ha ocurrido algo que no permitió procesar su orden...' mod='mtspayuapi'}</h2>
	<p>
		{l s='Has intentado realizar una compra en nuestra tienda en linea con Tarjeta de Crédito a través PayU, pero algo salió mal y ' mod='mtspayuapi'}<u>{l s='el pago no se pudo realizar.' mod='mtspayuapi'}</u>
	</p>
	<br>

{elseif $resultQuery == 'error'}

	{*<h2 class="gracias-text">{l s='Algo no salió bien...' mod='mtspayuapi'}</h2>*}
    <h2 class="gracias-text">{l s='Ha ocurrido algo que no permitió procesar su orden...' mod='mtspayuapi'}</h2>
	<p>
		{l s='Has intentado realizar una compra en nuestra tienda en linea con Tarjeta de Crédito a través PayU, pero algo salió mal y ' mod='mtspayuapi'}<u>{l s='el pago no se pudo realizar.' mod='mtspayuapi'}</u>
	</p>
	<br>

{else}
	
	<h1 class="page-heading">{l s='¿Qué haces por aquí?' mod='mtspayuapi'}</h1>

{/if}

	<h3 class="mts-text-c m-top-negativo">{l s='Resumen de la transacción' mod='mtspayuapi'}</h3>
	<table border="1" id="mts_payment_result">
		<tr>
			<th class="mts_payment_result_header" colspan="2">{l s='Resumen de la operación realizada con Tarjeta de Crédito' mod='mtspayuapi'}</th>
		</tr>
		{if isset($companyName)}
		<tr>
			<td><strong>{l s='Empresa' mod='mtspayuapi'}</strong></td>
			<td>{$companyName}</td>
		</tr>
		{/if}
		{if isset($companyNit)}
		<tr>
			<td><strong>{l s='NIT' mod='mtspayuapi'}</strong></td>
			<td>{$companyNit}</td>
		</tr>
		{/if}
		{if isset($orderID)}			
		<tr>
			<td><strong>{l s='Código de orden de PayU' mod='mtspayuapi'}</strong></td>
			<td>{$orderID}</td>
		</tr>
		{/if}
		{if isset($transactionDate)}			
		<tr>
			<td><strong>{l s='Fecha' mod='mtspayuapi'}</strong></td>
			<td>{$transactionDate}</td>
		</tr>
		{/if}
		{if isset($referenceOrder)}	
		<tr>
			<td><strong>{l s='Referencia de orden' mod='mtspayuapi'}</strong></td>
			<td>{$referenceOrder}</td>
		</tr>
		{/if}
		{if isset($transactionId)}	
		<tr>
			<td><strong>{l s='Identificador de transacción' mod='mtspayuapi'}</strong></td>
			<td>{$transactionId}</td>
		</tr>
		{/if}
		{if isset($paymentNetworkResponse)}	
		<tr>
			<td><strong>{l s='Mensaje de la red de pagos' mod='mtspayuapi'}</strong></td>
			<td>{$paymentNetworkResponse}</td>
		</tr>
		{/if}
		{if isset($trazabilityCode)}	
		<tr>
			<td><strong>{l s='Código de trazabilidad' mod='mtspayuapi'}</strong></td>
			<td>{$trazabilityCode}</td>
		</tr>
		{/if}
		{if isset($errorMessage)}	
		<tr>
			<td><strong>{l s='Mensaje' mod='mtspayuapi'}</strong></td>
			<td>{$errorMessage}</td>
		</tr>
		{/if}
		{if isset($authorizationCode)}	
		<tr>
			<td><strong>{l s='Código de autorización' mod='mtspayuapi'}</strong></td>
			<td>{$authorizationCode}</td>
		</tr>
		{/if}
		{if isset($pendingReason)}	
		<tr>
			<td><strong>{l s='Motivo de pago pendiente' mod='mtspayuapi'}</strong></td>
			<td>{$pendingReason}</td>
		</tr>
		{/if}
		{if isset($value) && isset($currency)}	
		<tr>
			<td><strong>{l s='Valor' mod='mtspayuapi'}</strong></td>
			<td>$ {$value|number_format:0} {$currency}</td>
		</tr>
		{/if}
		<tr>
			<th class="mts_payment_result_header" colspan="2"><strong style="color:red">IMPORTANTE:</strong> {l s='Este consumo se reflejará en sus estados de cuenta de tarjeta de crédito como: PayU pagos Online' mod='mtspayuapi'}</th>
		</tr>
	</table>

	<div id="cart_navigation">
		<div>
			<a class="button btn btn-default standard-checkout button-small f-right payment-btn" href="/order-history">Ir a Historial de Pedidos</a>
		</div>
	</div>

{if ($resultQuery == 'success' && $paymentState == 'declined') || $resultQuery == 'error'}

	{*<div>*}
    <div id="cart_navigation">
		{*<a class="payment_sub" href="/order-quick?submitReorder=&id_order={$id_order}">Reintentar compra</a>*}
        <a class="button btn btn-default standard-checkout button-small payment-btn" href="/order-quick?submitReorder=&id_order={$id_order}">Reintentar compra</a>
	</div>

{/if}

<!-- link js -->
<script type="application/javascript" src="{$modules_dir|escape:'html':'UTF-8'}mtspayuapi/views/templates/js/jQuery.print.js"></script> 
<!-- Fin Link js -->

{if $resultQuery == 'success' && $paymentState == 'approved'}

<script type="text/javascript"> 
	var gr_goal_params = {
	param_0 : '',
	param_1 : '',
	param_2 : '',
	param_3 : '',
	param_4 : '',
	param_5 : ''
};</script>
<script type="text/javascript" src="https://app.getresponse.com/goals_log.js?p=1212401&u=BZ5Yi"></script>
<script type="text/javascript">
	var valorTotaldeCompraExitosa = 100.000;
	var currency = "USD";
	fbq('track', 'Purchase', { value: [valorTotaldeCompraExitosa], currency: [currency] });
</script>
{elseif $resultQuery == 'success' && $paymentState == 'pending'}
<script type="text/javascript"> 
	var gr_goal_params = {
	param_0 : '',
	param_1 : '',
	param_2 : '',
	param_3 : '',
	param_4 : '',
	param_5 : ''
};</script>
<script type="text/javascript" src="https://app.getresponse.com/goals_log.js?p=1212401&u=BZ5Yi"></script>
<script type="text/javascript">
	var valorTotaldeCompraExitosa = 100.000;
	var currency = "USD";
	fbq('track', 'Purchase', { value: [valorTotaldeCompraExitosa], currency: [currency] });
</script>	
{/if}