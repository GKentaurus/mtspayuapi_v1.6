{*
* 2016 Metasysco
*
* AVISO DE LICENCIA
*
* Este archivo fuente está sujeto a la Academic Free License (AFL 3.0)
* El cual está incluido en el archivo LICENCE.txt.
* También se encuentra disponible en línea, en la siguiente URL:
* http://opensource.org/licenses/afl-3.0.php
* Si por algún motivo usted no recibió una copia de esta licencia,
* o no pudo obtenerlo a través de la URL, por favor envíe un correo a
* info@metasysco.com, y en la brevedad de lo posible se le enviará una
* copia inmediata.
*
* ADVERTENCIA
*
* No edite, modifique o altére el código de este archivo, si usted
* está tiene planeado a futuro actualizar la plataforma Prestashop 
* a una nueva versión (Aplicable para la versión de prestashop 1.6.x.x).
* Si usted desea modificar este módulo para su necesidad, por favor
* contactenos por medio del correo electrónico development@metasysco.com
* o visite nuestra página web http://www.metasysco.com para mas información.
*
* @author Carlos Moreno <carlos.moreno@metasysco.com.co>
* @copyright 2016 Metasysco S.A.S.
* @version 1.1.0
* @license http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0) 
*}

<h1 class="page-heading">{l s='Resultado de la operación' mod='mtspayuapi'}</h1>

<div id="mtspayuapi-efecty-baloto">
	<div id="dvContents">

	{if $resultQuery == 'success' && $paymentState == 'approved'}

		<h2 class="gracias-text">{l s='Gracias por su compra!' mod='mtspayuapi'}</h2>
		<p>
			{l s='Has solicitado realizar el pago de la compra que realizaste en nuestra tienda en línea, a través de Baloto con PayU.' mod='mtspayuapi'}
		</p>
		<br>

	{elseif $resultQuery == 'success' && $paymentState == 'pending'}

		<h2 class="gracias-text">{l s='Gracias por tu compra!' mod='mtspayuapi'}</h2>
		<p>
			{l s='Has solicitado realizar el pago de la compra que realizaste en nuestra tienda en línea, a través de Baloto con PayU.' mod='mtspayuapi'} <u>{l s='Estamos esperando la confirmación de pago para proceder con el envío.' mod='mtspayuapi'}</u>
		</p>
		<br>

	{elseif $resultQuery == 'success' && $paymentState == 'declined'}

        {*<h2 class="gracias-text">{l s='Algo no salió bien...' mod='mtspayuapi'}</h2>*}
        <h2 class="gracias-text">{l s='Ha ocurrido algo que no permitió procesar su orden...' mod='mtspayuapi'}</h2>
		<p>
			{l s='Has solicitado realizar el pago de la compra que realizaste en nuestra tienda en línea, a través de Baloto con PayU, pero algo salió mal y el código de pago' mod='mtspayuapi'} <u>{l s='no se pudo realizar.' mod='mtspayuapi'}</u>
		</p>
		<br>

	{elseif $resultQuery == 'error'}

		{*<h2 class="gracias-text">{l s='Algo no salió bien...' mod='mtspayuapi'}</h2>*}
        <h2 class="gracias-text">{l s='Ha ocurrido algo que no permitió procesar su orden...' mod='mtspayuapi'}</h2>
		<p>
			{l s='Has solicitado realizar el pago de la compra que realizaste en nuestra tienda en línea, a través de Baloto con PayU, pero algo salió mal y el código de pago' mod='mtspayuapi'} <u>{l s='no se pudo realizar.' mod='mtspayuapi'}</u>
		</p>
		<br>

	{else}
		
		<h1 class="page-heading">{l s='¿Qué haces por aquí?' mod='mtspayuapi'}</h1>

	{/if}

		<h3 class="mts-text-c m-top-negativo">{l s='Resumen de la transacción' mod='mtspayuapi'}</h3>
		<table border="1" id="mts_payment_result">
			<tr>
				<th class="mts_payment_result_header" colspan="2">{l s='Resumen de la operación realizada con Baloto' mod='mtspayuapi'}</th>
			</tr>
			{if isset($companyName)}
			<tr>
				<td><strong>{l s='Empresa' mod='mtspayuapi'}</strong></td>
				<td>{$companyName}</td>
			</tr>
			{/if}
			{if isset($companyNit)}
			<tr>
				<td><strong>{l s='NIT' mod='mtspayuapi'}</strong></td>
				<td>{$companyNit}</td>
			</tr>
			{/if}
			{if isset($paymentReference)}			
			<tr>
				<td><strong style="color:#85b350">{l s='Referencia de Pago de PayU' mod='mtspayuapi'}</strong></td>
				<td><strong style="color:#85b350">{$paymentReference}</strong></td>
			</tr>
            <tr>
				<td><strong style="color:#85b350">{l s='Número de Convenio' mod='mtspayuapi'}</strong></td>
				<td><strong style="color:#85b350">950110</strong></td>
			</tr>
			{/if}
			{if isset($referenceOrder)}	
			<tr>
				<td><strong>{l s='Referencia de orden' mod='mtspayuapi'}</strong></td>
				<td>{$referenceOrder}</td>
			</tr>
			{/if}
			{if isset($value) && isset($currency)}	
			<tr>
				<td><strong>{l s='Valor' mod='mtspayuapi'}</strong></td>
				<td>$ {$value|number_format:0} {$currency}</td>
			</tr>
			{/if}
			{if isset($payUntilDay) && isset($payUntilHour)}			
			<tr>
				<td><strong>{l s='Plazo máximo de pago' mod='mtspayuapi'}</strong></td>
				<td>{l s='Hasta el día' mod='mtspayuapi'} {$payUntilDay} {l s='a las' mod='mtspayuapi'} {$payUntilHour}</td>
			</tr>
			{/if}
			{if isset($transactionId)}	
			<tr>
				<td><strong>{l s='Identificador de transacción' mod='mtspayuapi'}</strong></td>
				<td>{$transactionId}</td>
			</tr>
			{/if}
			{if isset($trazabilityCode)}	
			<tr>
				<td><strong>{l s='Código de trazabilidad' mod='mtspayuapi'}</strong></td>
				<td>{$trazabilityCode}</td>
			</tr>
			{/if}
			{if isset($errorMessage)}	
			<tr>
				<td><strong>{l s='Mensaje' mod='mtspayuapi'}</strong></td>
				<td>{$errorMessage}</td>
			</tr>
			{/if}
			{if isset($transactionState)}	
			<tr>
				<td><strong>{l s='Estado de la transacción' mod='mtspayuapi'}</strong></td>
				<td>{$transactionState}</td>
			</tr>
			{/if}
			<tr>
				<th colspan="2" class="pay_important" style="text-align:justify">
					<strong>{l s='Importante:' mod='mtspayuapi'}</strong> {l s='Recuerde que puede acercarse a cualquier establecimiento ' mod='mtspayuapi'}<strong style="text-transform: uppercase;">{$paymentMethod}</strong>{l s=' con solo presentar el número de' mod='mtspayuapi'} <strong>{l s='Referencia de Pago de PayU y Número de Convenio' mod='mtspayuapi'}</strong>{l s='. Dispone de ' mod='mtspayuapi'}{if $paymentMethod == "baloto"}3{elseif $paymentMethod == 'efecty'}5{/if}{l s=' días hábiles.' mod='mtspayuapi'}
				</th>
			</tr>
		</table>

		<div id="cart_navigation" class="cart_navigation efecty_baloto">
			<div>
				<a class="button btn btn-default standard-checkout button-small f-right payment-btn" href="/order-history">Ir a Historial de Pedidos</a>
			</div>
			<div class="pay_inline">
				<!-- hidden xs -->
				<a class="hidden-xs button btn btn-default standard-checkout button-small payment-btn" target="_blank" href="{$urlPaymentPDF}" style="float: left">Descargar comprobante</a>
				<!-- visible xs -->
				<a class="visible-xs button btn btn-default standard-checkout button-small payment-btn " target="_blank" href="{$urlPaymentPDF}" style="float: left">Comprobante</a>
			</div>
			<div class="pay_block">
				<a class="payment_sub button-exclusive btn btn-default" target="_blank" href="{$urlPaymentHTML}" ><i class="icon-chevron-left" style="line-height: 25px;"></i>Ver en PayU</a>
			</div>
		</div>
	{if ($resultQuery == 'success' && $paymentState == 'declined') || $resultQuery == 'error'}

		{*<div>*}
        <div id="cart_navigation">
			{*<a class="payment_sub" href="/order-quick?submitReorder=&id_order={$id_order}">Reintentar compra</a>*}
            <a class="button btn btn-default standard-checkout button-small payment-btn" href="/order-quick?submitReorder=&id_order={$id_order}">Reintentar compra</a>
		</div>

	{/if}
</div>

<!-- link js -->
<script type="application/javascript" src="{$modules_dir|escape:'html':'UTF-8'}mtspayuapi/views/templates/js/jQuery.print.js"></script> 
<!-- Fin Link js -->

{if $resultQuery == 'success' && $paymentState == 'approved'}
<script type="text/javascript"> 
	var gr_goal_params = {
	param_0 : '',
	param_1 : '',
	param_2 : '',
	param_3 : '',
	param_4 : '',
	param_5 : ''
};</script>
<script type="text/javascript" src="https://app.getresponse.com/goals_log.js?p=1212401&u=BZ5Yi"></script>
<script type="text/javascript">
	var valorTotaldeCompraExitosa = 100.000;
	var currency = "USD";
	fbq('track', 'Purchase', { value: [valorTotaldeCompraExitosa], currency: [currency] });
</script>
{elseif $resultQuery == 'success' && $paymentState == 'pending'}
<script type="text/javascript"> 
	var gr_goal_params = {
	param_0 : '',
	param_1 : '',
	param_2 : '',
	param_3 : '',
	param_4 : '',
	param_5 : ''
};</script>
<script type="text/javascript" src="https://app.getresponse.com/goals_log.js?p=1212401&u=BZ5Yi"></script>
<script type="text/javascript">
	var valorTotaldeCompraExitosa = 100.000;
	var currency = "USD";
	fbq('track', 'Purchase', { value: [valorTotaldeCompraExitosa], currency: [currency] });
</script>	
{/if}