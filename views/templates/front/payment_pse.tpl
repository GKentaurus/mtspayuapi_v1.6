{*
* 2016 Metasysco
*
* AVISO DE LICENCIA
*
* Este archivo fuente está sujeto a la Academic Free License (AFL 3.0)
* El cual está incluido en el archivo LICENCE.txt.
* También se encuentra disponible en línea, en la siguiente URL:
* http://opensource.org/licenses/afl-3.0.php
* Si por algún motivo usted no recibió una copia de esta licencia,
* o no pudo obtenerlo a través de la URL, por favor envíe un correo a
* info@metasysco.com, y en la brevedad de lo posible se le enviará una
* copia inmediata.
*
* ADVERTENCIA
*
* No edite, modifique o altére el código de este archivo, si usted
* está tiene planeado a futuro actualizar la plataforma Prestashop 
* a una nueva versión (Aplicable para la versión de prestashop 1.6.x.x).
* Si usted desea modificar este módulo para su necesidad, por favor
* contactenos por medio del correo electrónico development@metasysco.com
* o visite nuestra página web http://www.metasysco.com para mas información.
*
* @author Carlos Moreno <carlos.moreno@metasysco.com.co>
* @copyright 2016 Metasysco S.A.S.
* @version 1.1.0
* @license http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0) 
*}

<!-- <script src="https://code.jquery.com/jquery-3.1.1.min.js" type="text/javascript" charset="utf-8" async defer></script> -->

{literal}
	<script>
		$('.mts-order').attr('id','order');
	</script>
{/literal}



{capture name=path}
	<a href="{$link->getPageLink('order', true, NULL, "step=3")|escape:'html':'UTF-8'}" title="{l s='Volver al Checkout' mod='mtspayuapi'}">{l s='Checkout' mod='mtspayuapi'}</a><span class="navigation-pipe">{$navigationPipe}</span>{l s='Pago a través de PayU' mod='mtspayuapi'}
{/capture}
<div id="module-mtspaymentapi-prevalidation">
	<h2 class="page-heading" >{l s='Total de la orden' mod='mtspayuapi'}</h2>

	{assign var='current_step' value='payment'}
	{include file="$tpl_dir./order-steps.tpl"}

	{if isset($nbProducts) && $nbProducts <= 0}
		<p class="warning">{l s='Tu carrito de compras está vacío.' mod='mtspayuapi'}</p>
	{else}

	<div class="col col-xs-12 col-sm-12 col-md-4 col-md-push-8">
		<h4 class="elgpt m-r-negativo"> {l s='Resumen de la compra' mod='mtspayuapi'} </h4>
		<div class="box lgpt-box m-l-negativo m-r-negativo">
			<p> {l s='Productos:' mod='mtspayuapi'} </p>
			<ul class="m-l-plus">
				{foreach name=outer item=product from=$products}
					{foreach name=inner key=key item=item from=$product}
						{if $key == 'name'}
							<li style="list-style: disc;">{$item}</li>
						{/if}
					{/foreach}				
				{/foreach}
			</ul>
			<p class="payment_total">
				<span class="first">{l s='Total a pagar:' mod='mtspayuapi'}</span>
				<span class="payu-puntos"></span>
				<span class="last">$ {$total|number_format:0} {$currency}</span>
			</p>
			<p class="pay_ship">
				<strong>{l s='Envío incluido!' mod='mtspayuapi'}</strong>
			</p>
		</div>
	</div>

	<div class="col col-xs-12 col-sm-12 col-md-8 col-md-pull-4">
		<h3 class="elgpt m-r-megativo-xs">{l s='Has elegido el método de pago en línea de PayU' mod='mtspayuapi'}</h3>
		<div id="payment_pse" class="box lgpt-box m-r-megativo-xs">
			<div id="payment_error" class="alert alert-danger" style="display: none;">
				<p>
					{l s='Se ha presentado un error en la solicitud:' mod='mtspayuapi'}
				</p>
				<ul>
					<li id="error_bank" style="display: none;"> {l s='Seleccione un banco válido.' mod='mtspayuapi'}</li>
					<li id="error_name" style="display: none;"> {l s='Ingrese un nombre válido.' mod='mtspayuapi'}</li>
					<li id="error_type" style="display: none;"> {l s='Seleccione un tipo de persona válido.' mod='mtspayuapi'}</li>
					<li id="error_dnitype" style="display: none;"> {l s='Seleccione un tipo de documento válido.' mod='mtspayuapi'}</li>
					<li id="error_dni" style="display: none;"> {l s='Ingrese un documento válido.' mod='mtspayuapi'}</li>
					<li id="error_number" style="display: none;"> {l s='Ingrese un teléfono válido.' mod='mtspayuapi'}</li>
				</ul>
				{l s='Ingrese la información requerida nuevamente para proceder con el pago.' mod='mtspayuapi'}
			</div>
			<div class="cc-img">
				<h4>{l s='Pago con PSE:' mod='mtspayuapi'}</h4>
				<img class="payment_pse_pseimg" src="{$modules_dir|escape:'html':'UTF-8'}mtspayuapi/views/templates/img/icon-pse-2.png" alt="PSE">
			</div>
			<ul id="info_pse">
				<li> <div>1.</div> <p>{l s='Todas las compras y pagos por PSE son realizados en línea y la confirmación es inmediata.' mod='mtspayuapi'} </p> </li>
				<li> <div>2.</div> <p>{l s='Algunos bancos tienen un procedimiento de autenticación en su página (por ejemplo, una segunda clave), si nunca has realizado pagos por internet con tu cuenta de ahorros o corriente, es posible que necesites tramitar una autorización ante tu banco. Si tienes dudas, puedes consultar los' mod='mtspayuapi'} {if isset($helpPseMode) && $helpPseMode == 'true'}<a id="requires-banks" target="_blank" href="{$helpPseLink}">{l s='requisitos de cada banco.' mod='mtspayuapi'}</a>{else}{l s='requisitos de cada banco.' mod='mtspayuapi'}{/if}</p></li>
			</ul>
			<form id="form_pse" class="m-top-negativo" role="form" method="post" action="{$link->getModuleLink('mtspayuapi', 'pse_process', [], true)|escape:'html'}">

					<div style="background:url(https://maf.pagosonline.net/ws/fp?id={$deviceSessionId})"></div>
					<img class="img-none" src="https://maf.pagosonline.net/ws/fp/clear.png?id={$deviceSessionId}">
					<script src="https://maf.pagosonline.net/ws/fp/check.js?id={$deviceSessionId}"></script>
					<object type="application/x-shockwave-flash" data="https://maf.pagosonline.net/ws/fp/fp.swf?id={$deviceSessionId}" width="1" height="1" id="thm_fp">
						<param name="movie" value="https://maf.pagosonline.net/ws/fp/fp.swf?id={$deviceSessionId}" />
					</object>
							
					<p class="m-top-negativo mts-text-c	t-bold m-bot">{l s='Por favor diligencie el siguiente formulario:' mod='mtspayuapi'} </p>
					
					<input type="hidden" name="currency_payement" value="{$currencies.0.id_currency}" />
					<input type="hidden" id="deviceSessionId" name="deviceSessionId" value="{$deviceSessionId}" />
					
					<div class="payment_info"> 
						<div class="col col-xs-12 col-sm-6">
							<span> {l s='Seleccione su Banco:' mod='mtspayuapi'} </span>
						</div>
						<div class="col col-xs-12 col-sm-6">
							<select name="pse_bank" id="pse_bank">
								<option value="invalid">{l s='- Seleccione un banco -' mod='mtspayuapi'}</option>
							{foreach name=outher item=arrayBank from=$bankList}
								{foreach item=bankInfo from=$arrayBank}
									{foreach item=item key=key from=$bankInfo}					
										{if $key != "0" && $key == "description"}
											{assign "bancoNombre" $item}
										{/if}
										{if $key != "0" && $key == "pseCode"}
											{assign "bancoId" $item}
											<option value="{$bancoId}">{$bancoNombre}</option>
										{/if}					
									{/foreach}
								{/foreach}
							{/foreach}
							</select>
						</div>
					</div>
					<div class="payment_info"> 
						<div class="col col-xs-12 col-sm-6">
							<span> {l s='Nombre del Titular:' mod='mtspayuapi'} </span>
						</div>
						<div class="col col-xs-12 col-sm-6">
							<input type="text" name="pse_name" id="pse_name"/>
						</div>
					</div>
					<div class="payment_info">
						<div class="col col-xs-12 col-sm-6">
							<span> 	{l s='Tipo de persona:' mod='mtspayuapi'} </span>
						</div>
						<div class="col col-xs-12 col-sm-6 pre-duo">
							<div class="duo">
								<select name="pse_type" id="pse_type"/>
									<option value="invalid">{l s='- Seleccione una opción -' mod='mtspayuapi'}</option>
									<option value="N">{l s='Persona Natural' mod='mtspayuapi'}</option>
									<option value="J">{l s='Persona Juridica' mod='mtspayuapi'}</option>
								</select>
							</div>
						</div>
					</div>
					<div class="payment_info"> 
						<div class="col col-xs-12 col-sm-6">
							<span> {l s='Documento:' mod='mtspayuapi'} </span>
						</div>
						<div class="col col-xs-12 col-sm-6 pre-duo">
							<div class="duo">
								<select name="pse_dnitype" id="pse_dnitype">
									<option value="invalid">{l s='--' mod='mtspayuapi'}</option>
									<option value="CC">{l s='CC' mod='mtspayuapi'}</option>
									<option value="CE">{l s='CE' mod='mtspayuapi'}</option>
									<option value="NIT">{l s='NIT' mod='mtspayuapi'}</option>
									<option value="TI">{l s='TI' mod='mtspayuapi'}</option>
									<option value="PP">{l s='PP' mod='mtspayuapi'}</option>
									<option value="IDC">{l s='IDC' mod='mtspayuapi'}</option>
									<option value="CEL">{l s='CEL' mod='mtspayuapi'}</option>
									<option value="RC">{l s='RC' mod='mtspayuapi'}</option>
									<option value="DE">{l s='DE' mod='mtspayuapi'}</option>
								</select>
								<input type="text" name="pse_dni" id="pse_dni"/>
							</div>
						</div>
					</div>
					<div class="payment_info"> 
						<div class="col col-xs-12 col-sm-6">
							<span> {l s='Teléfono:' mod='mtspayuapi'}</span> 
						</div>
						<div class="col col-xs-12 col-sm-6">
							<input type="text" name="pse_phone" id="pse_phone"/>
						</div>
					</div>
				<p>
					{l s='Verifique la información en la siguiente pantalla.' mod='mtspayuapi'}
				</p>
					
				<!-- <p> <b>{l s='Por favor confirme su orden haciendo clic en  \'Pagar ahora\'.' mod='mtspayuapi'}</b>	</p> -->
			</form>
			<p class="cart_navigation" id="cart_navigation">
				<button id="sendPSE" class="button btn btn-default standard-checkout button-small f-right payment-btn">{l s='Pagar ahora' mod='mtspayuapi'}</button>
				<a href="{$link->getPageLink('order', true, NULL, "step=3")|escape:'html'}#opc_payment_methods" class="button-exclusive btn btn-default" style="line-height: 2"><i class="icon-chevron-left" style="line-height: 2;"></i>{l s='Volver a métodos de pago' mod='mtspayuapi'}</a>
			</p>
		</div>
	</div>
</div>
{/if}
