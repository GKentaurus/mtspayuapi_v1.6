{*
* 2016 Metasysco
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to info@metasysco.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Metasysco S.A.S. <contacto@metasysco.com.co>
*  @copyright  2016 Metasysco S.A.S.
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of Metasysco S.A.S.
*}

<div class="alert alert-info">
<img src="../modules/mtspayuapi/logo.png" style="float:left; margin-right:15px;">
<p><strong>{l s="Este módulo le permite configurar una cuenta de PayU Latam a través de la API." mod='mtspayuapi'}</strong></p>
<p>{l s="Si el cliente elige este método de pago, todas las transacciones, excepto PSE, se mantendrán en la misma página o serán redireccionadas a la tienda en línea." mod='mtspayuapi'}</p>
<p>{l s="Los enlaces de apoyo en cualquier método de pago, son opcionales." mod='mtspayuapi'}</p>
</div>
