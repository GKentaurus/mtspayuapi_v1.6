{*
* 2016 Metasysco
*
* NOTICE OF LICENSE
*
* This source file is subject to the Academic Free License (AFL 3.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/afl-3.0.php
* If you did not receive a copy of the license and are unable to
* obtain it through the world-wide-web, please send an email
* to info@metasysco.com so we can send you a copy immediately.
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade PrestaShop to newer
* versions in the future. If you wish to customize PrestaShop for your
* needs please refer to http://www.prestashop.com for more information.
*
*  @author Metasysco S.A.S. <contacto@metasysco.com.co>
*  @copyright  2016 Metasysco S.A.S.
*  @license    http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0)
*  International Registered Trademark & Property of Metasysco S.A.S.
*}

<h1 class="page-heading" style="margin-top:0;">Elija forma de pago</h1>

<div id="mts_payupayment">
	<div class="col col-xs-12 col-sm-8 col-md-6">
		<a href="{$link->getModuleLink('mtspayuapi', 'card_option', [], true)|escape:'html'}" title="{l s='Pague con su Tarjeta Crédito' mod='mtspayuapi'}">		
			<div class="row no-m-r">
				<div class="payment-cont max-cont" style="text-align: center;">
					<div class="col col-xs-12" style="text-align: center; margin-top: -20px;">
						<p class="under-p visible-xs">
							<b>{l s='Tarjeta de Crédito' mod='mtspaymentapi'}</b>
						</p>
						<p class="under-p hidden-xs">
							<b>{l s='Pague con su Tarjeta Crédito o Débito' mod='mtspaymentapi'}</b>
						</p>
						<p class="multiple-img">
							<img src="{$modules_dir|escape:'html':'UTF-8'}mtspayuapi/views/templates/img/icon-visa.png" alt="VISA">
							<img src="{$modules_dir|escape:'html':'UTF-8'}mtspayuapi/views/templates/img/icon-mastercard.png" alt="Master Card">
							<img src="{$modules_dir|escape:'html':'UTF-8'}mtspayuapi/views/templates/img/icon-americanexpress.png" alt="American Express">
							<img src="{$modules_dir|escape:'html':'UTF-8'}mtspayuapi/views/templates/img/icon-dinersclub.png" alt="Diners Club">
							<img src="{$modules_dir|escape:'html':'UTF-8'}mtspayuapi/views/templates/img/icon-codensa.png" alt="Codensa">
						</p>	
					</div>
					<i class="fa fa-chevron-right payment-arrow" style="top:52%;"></i>
				</div>
			</div>
		</a>
	</div>
	<div class="col col-xs-6 col-sm-4 col-md-3">
		<a href="{$link->getModuleLink('mtspayuapi', 'pse_option', [], true)|escape:'html'}" title="{l s='Pague a través de PSE' mod='mtspayuapi'}">		
			<div class="row no-m-r">
				<div class="payment-cont f-arrow" style="text-align: center;">
					<div class="col col-xs-12 payment-top sec" style="text-align: center; margin-top: -20px;">
						<p class="under-p visible-xs">
							<b>{l s='PSE' mod='mtspaymentapi'}</b>
						</p>
						<p class="under-p hidden-xs">
							<b>{l s='Pague a través de PSE' mod='mtspaymentapi'}</b>
						</p>
						<p>
							<img class="pse" src="{$modules_dir|escape:'html':'UTF-8'}mtspayuapi/views/templates/img/icon-pse-2.png" alt="PSE">
						</p>	
					</div>
					<i class="fa fa-chevron-right payment-arrow" style="top:52%;"></i>
				</div>
			</div>
		</a>
	</div>
	<div class="col col-xs-6 col-sm-4 col-md-3">
		<a href="{$link->getModuleLink('mtspayuapi', 'efecty_option', [], true)|escape:'html'}" title="{l s='Pague con Efecty' mod='mtspayuapi'}">		
			<div class="row no-m-r">
				<div class="payment-cont f-arrow" style="text-align: center;">
					<div class="col col-xs-12 payment-top sec" style="text-align: center; margin-top: -20px;">
						<p class="under-p visible-xs">
							<b>{l s='Efecty' mod='mtspaymentapi'}</b>
						</p>
						<p class="under-p hidden-xs">
							<b>{l s='Pague con Efecty' mod='mtspaymentapi'}</b>
						</p>
						<p>
							<img class="efecty" src="{$modules_dir|escape:'html':'UTF-8'}mtspayuapi/views/templates/img/icon-efecty-2.png" alt="Efecty">
						</p>	
					</div>
					<i class="fa fa-chevron-right payment-arrow" style="top:52%;"></i>
				</div>
			</div>
		</a>
	</div>
	<div class="col col-xs-6 col-sm-4 col-md-3">
		<a href="{$link->getModuleLink('mtspayuapi', 'baloto_option', [], true)|escape:'html'}" title="{l s='Pague con Baloto' mod='mtspayuapi'}">		
			<div class="row no-m-r">
				<div class="payment-cont f-arrow" style="text-align: center;">
					<div class="col col-xs-12 payment-top sec" style="text-align: center; margin-top: -20px;">
						<p class="under-p visible-xs">
							<b>{l s='Baloto' mod='mtspaymentapi'}</b>
						</p>
						<p class="under-p hidden-xs">
							<b>{l s='Pague con Baloto' mod='mtspaymentapi'}</b>
						</p>
						<p class="visible-xs">
							<img class="baloto" src="{$modules_dir|escape:'html':'UTF-8'}mtspayuapi/views/templates/img/baloto-logo.png" alt="Baloto">
						</p>
						<p class="hidden-xs">
							<img class="baloto" src="{$modules_dir|escape:'html':'UTF-8'}mtspayuapi/views/templates/img/icon-baloto-2.png" alt="Baloto">
						</p>	
					</div>
					<i class="fa fa-chevron-right payment-arrow" style="top:52%;"></i>
				</div>
			</div>
		</a>
	</div>
</div>

	<!-- <script>		
		$(document).ready(function(){
			$(window).resize(function(){

				var maxcont = $('.payment-cont.max-cont').outerHeight();

				$('.payment-cont.h-cont').css({
					'min-height': ( maxcont + 'px')
 				});

			}); 
		});
	</script> -->