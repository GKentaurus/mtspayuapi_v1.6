<?php
/**
* 2016 Metasysco
*
* AVISO DE LICENCIA
*
* Este archivo fuente está sujeto a la Academic Free License (AFL 3.0)
* El cual está incluido en el archivo LICENCE.txt.
* También se encuentra disponible en línea, en la siguiente URL:
* http://opensource.org/licenses/afl-3.0.php
* Si por algún motivo usted no recibió una copia de esta licencia,
* o no pudo obtenerlo a través de la URL, por favor envíe un correo a
* info@metasysco.com, y en la brevedad de lo posible se le enviará una
* copia inmediata.
*
* ADVERTENCIA
*
* No edite, modifique o altére el código de este archivo, si usted
* está tiene planeado a futuro actualizar la plataforma Prestashop 
* a una nueva versión (Aplicable para la versión de prestashop 1.6.x.x).
* Si usted desea modificar este módulo para su necesidad, por favor
* contactenos por medio del correo electrónico development@metasysco.com
* o visite nuestra página web http://www.metasysco.com para mas información.
*
* @author Carlos Moreno <carlos.moreno@metasysco.com.co>
* @copyright 2016 Metasysco S.A.S.
* @version 1.1.0
* @license http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0) 
*/

class MtsPayuApiNotificationModuleFrontController extends ModuleFrontController 
{
	public function initContent()
	{
		include_once(_PS_MODULE_DIR_.'../config/config.inc.php');
		include_once(_PS_MODULE_DIR_.'../config/settings.inc.php');
		include_once(_PS_MODULE_DIR_.'../classes/Cookie.php');
		include_once(_PS_MODULE_DIR_.'../classes/order/OrderHistory.php');
		include_once(_PS_MODULE_DIR_.'../classes/order/Order.php');
		include_once(_PS_MODULE_DIR_.'mtspayuapi/mailer/PHPMailerAutoload.php');
		parent::initContent();

		// Consultamos la referencia de Prestashop en la Base de Datos
		if (isset($_POST['reference_sale']) && !empty($_POST['reference_sale']))
		{
			$referenceSale = $_POST['reference_sale'];
			$sql = 'SELECT * FROM `'._DB_PREFIX_.'orders` WHERE `reference` = "'.$referenceSale.'" ORDER BY `reference` DESC';
			$order = Db::getInstance()->executeS($sql);

			$moduleOS = [
				'pending' => [
					'card' => Configuration::get('PS_OS_MTS_PAYU_PENDING_CARD'),
					'pse' => Configuration::get('PS_OS_MTS_PAYU_PENDING_PSE'),
					'efecty' => Configuration::get('PS_OS_MTS_PAYU_PENDING_EFECTY'),
					'baloto' => Configuration::get('PS_OS_MTS_PAYU_PENDING_BALOTO')
				],
				'payed' => [
					'card' => Configuration::get('PS_OS_MTS_PAYU_PAID_CARD'),
					'pse' => Configuration::get('PS_OS_MTS_PAYU_PAID_PSE'),
					'efecty' => Configuration::get('PS_OS_MTS_PAYU_PAID_EFECTY'),
					'baloto' => Configuration::get('PS_OS_MTS_PAYU_PAID_BALOTO')
				],
				'canceled' => Configuration::get('PS_OS_CANCELED')
			];

			if ($_POST['response_message_pol'] == 'APPROVED')
			{
				for ($i=0; $i < count($order); $i++)
				{ 					
					if ($order[$i]['current_state'] == $moduleOS['pending']['card'])
					{
						$objOrder = new Order($order[$i]['id_order']);
						$objOrder->setCurrentState($moduleOS['payed']['card']);
					}
					elseif ($order[$i]['current_state'] == $moduleOS['pending']['pse'])
					{
						$objOrder = new Order($order[$i]['id_order']);
						$objOrder->setCurrentState($moduleOS['payed']['pse']);
					}
					elseif ($order[$i]['current_state'] == $moduleOS['pending']['efecty'])
					{
						$objOrder = new Order($order[$i]['id_order']);
						$objOrder->setCurrentState($moduleOS['payed']['efecty']);
					}
					elseif ($order[$i]['current_state'] == $moduleOS['pending']['baloto'])
					{
						$objOrder = new Order($order[$i]['id_order']);
						$objOrder->setCurrentState($moduleOS['payed']['baloto']);
					}
				}
			}
			else
			{
				for ($i=0; $i < count($order); $i++)
				{
					$objOrder = new Order($order[$i]['id_order']);
					$objOrder->setCurrentState($moduleOS['canceled']);
				}
			}	
		}	
	}
}