<?php
/**
* 2016 Metasysco
*
* AVISO DE LICENCIA
*
* Este archivo fuente está sujeto a la Academic Free License (AFL 3.0)
* El cual está incluido en el archivo LICENCE.txt.
* También se encuentra disponible en línea, en la siguiente URL:
* http://opensource.org/licenses/afl-3.0.php
* Si por algún motivo usted no recibió una copia de esta licencia,
* o no pudo obtenerlo a través de la URL, por favor envíe un correo a
* info@metasysco.com, y en la brevedad de lo posible se le enviará una
* copia inmediata.
*
* ADVERTENCIA
*
* No edite, modifique o altére el código de este archivo, si usted
* está tiene planeado a futuro actualizar la plataforma Prestashop 
* a una nueva versión (Aplicable para la versión de prestashop 1.6.x.x).
* Si usted desea modificar este módulo para su necesidad, por favor
* contactenos por medio del correo electrónico development@metasysco.com
* o visite nuestra página web http://www.metasysco.com para mas información.
*
* @author Carlos Moreno <carlos.moreno@metasysco.com.co>
* @copyright 2016 Metasysco S.A.S.
* @version 1.1.0
* @license http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0) 
*/
class MtsPayuApiCard_ValidateModuleFrontController extends ModuleFrontController
{
	public function postProcess()
	{
	
		include_once(_PS_MODULE_DIR_.'../config/config.inc.php');
		include_once(_PS_MODULE_DIR_.'../config/settings.inc.php');
		include_once(_PS_MODULE_DIR_.'../classes/Cookie.php');
		include_once(_PS_MODULE_DIR_.'../classes/order/OrderHistory.php');
		include_once(_PS_MODULE_DIR_.'../classes/order/Order.php');
		parent::initContent();

		$cart = $this->context->cart;

		if ($cart->id_customer == 0 || $cart->id_address_delivery == 0 || $cart->id_address_invoice == 0 || !$this->module->active || empty($cart->getProducts()))
		{
			$array = ['redirect' => 'index.php?controller=order&step=1'];
			$arrayJson = json_encode($array);
			print_r($arrayJson);
			die();
		}

		// Datos de la tarjeta de crédito		
		$cc_franchise = filter_var($_POST['cc_franchise'], FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES); 	//String
		$cc_name = filter_var($_POST['cc_name'], FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);	//String
		$cc_dnitype = filter_var($_POST['cc_dnitype'], FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);	//String
		$cc_dni = filter_var($_POST['cc_dni'], FILTER_SANITIZE_NUMBER_INT);	//Numeric	
		$cc_number = filter_var($_POST['cc_number'], FILTER_SANITIZE_NUMBER_INT);	//Numeric
		$cc_securityCode = filter_var($_POST['cc_securityCode'], FILTER_SANITIZE_NUMBER_INT);	//Numeric
		$cc_installments = filter_var($_POST['cc_installments'], FILTER_SANITIZE_NUMBER_INT);	//Numeric
		$cc_expDateMonth = filter_var($_POST['cc_expDateMonth'], FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);	//Numeric
		$cc_expDateYear = filter_var($_POST['cc_expDateYear'], FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);	//Numeric		

		$cc_errors = [];

		if ($cc_franchise == false || $cc_franchise == 'invalid' || $cc_franchise == '')
		{
			$cc_errors['cc_franchise'] = 'Failed';
		}
		else
		{
			$cc_errors['cc_franchise'] = 'Ok';
		}

		if ($cc_name == false || $cc_name == '')
		{
			$cc_errors['cc_name'] = 'Failed';
		}
		else
		{
			$cc_errors['cc_name'] = 'Ok';
		}

		if ($cc_dnitype == false || $cc_dnitype == 'invalid' || $cc_dnitype == '')
		{
			$cc_errors['cc_dnitype'] = 'Failed';
		}
		else
		{
			$cc_errors['cc_dnitype'] = 'Ok';
		}

		if ($cc_dni == false || $cc_dni == '')
		{
			$cc_errors['cc_dni'] = 'Failed';
		}
		else
		{
			$cc_errors['cc_dni'] = 'Ok';
		}

		if ($cc_number == false || $cc_number == '')
		{
			$cc_errors['cc_number'] = 'Failed';
		}
		else
		{
			$cc_errors['cc_number'] = 'Ok';
		}

		if ($cc_securityCode == false || $cc_securityCode == '')
		{
			$cc_errors['cc_securityCode'] = 'Failed';
		}
		else
		{
			$cc_errors['cc_securityCode'] = 'Ok';
		}

		if ($cc_installments == false || $cc_installments == 'invalid' || $cc_installments == '')
		{
			$cc_errors['cc_installments'] = 'Failed';
		}
		else
		{
			$cc_errors['cc_installments'] = 'Ok';
		}

		if ($cc_expDateMonth == false || $cc_expDateMonth == '' || $cc_expDateMonth == 'invalid')
		{
			$cc_errors['cc_expDateMonth'] = 'Failed';
		}
		else
		{
			$cc_errors['cc_expDateMonth'] = 'Ok';
		}

		if ($cc_expDateYear == false || $cc_expDateYear == '' || $cc_expDateYear == 'invalid')
		{
			$cc_errors['cc_expDateYear'] = 'Failed';
		}
		else
		{
			$cc_errors['cc_expDateYear'] = 'Ok';
		}

		if (array_search('Failed', $cc_errors)) {
			$json = json_encode($cc_errors);
			print_r($json);
			die();
		}
		else
		{
			die();
		}
	}
}
