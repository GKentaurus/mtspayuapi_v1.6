<?php
/**
* 2016 Metasysco
*
* AVISO DE LICENCIA
*
* Este archivo fuente está sujeto a la Academic Free License (AFL 3.0)
* El cual está incluido en el archivo LICENCE.txt.
* También se encuentra disponible en línea, en la siguiente URL:
* http://opensource.org/licenses/afl-3.0.php
* Si por algún motivo usted no recibió una copia de esta licencia,
* o no pudo obtenerlo a través de la URL, por favor envíe un correo a
* info@metasysco.com, y en la brevedad de lo posible se le enviará una
* copia inmediata.
*
* ADVERTENCIA
*
* No edite, modifique o altére el código de este archivo, si usted
* está tiene planeado a futuro actualizar la plataforma Prestashop 
* a una nueva versión (Aplicable para la versión de prestashop 1.6.x.x).
* Si usted desea modificar este módulo para su necesidad, por favor
* contactenos por medio del correo electrónico development@metasysco.com
* o visite nuestra página web http://www.metasysco.com para mas información.
*
* @author Carlos Moreno <carlos.moreno@metasysco.com.co>
* @copyright 2016 Metasysco S.A.S.
* @version 1.1.0
* @license http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0) 
*/
class MtsPayuApiEfecty_ProcessModuleFrontController extends ModuleFrontController
{
	public function postProcess()
	{
	
		include_once(_PS_MODULE_DIR_.'../config/config.inc.php');
		include_once(_PS_MODULE_DIR_.'../config/settings.inc.php');
		include_once(_PS_MODULE_DIR_.'../classes/Cookie.php');
		include_once(_PS_MODULE_DIR_.'../classes/order/OrderHistory.php');
		include_once(_PS_MODULE_DIR_.'../classes/order/Order.php');
		parent::initContent();

		$cart = $this->context->cart;
		
		if ($cart->id_customer == 0 || $cart->id_address_delivery == 0 || $cart->id_address_invoice == 0 || !$this->module->active || empty($cart->getProducts()))
		{
			Tools::redirect('index.php?controller=order&step=1');
		}

		// Check that this payment option is still available in case the customer changed his address just before the end of the checkout process
		$authorized = false;
		foreach (Module::getPaymentModules() as $module)
			if ($module['name'] == 'mtspayuapi')
			{
				$authorized = true;
				break;
			}

		if (!$authorized)
			die($this->module->l('Este método de pago no está disponible.', 'mtspayuapi'));

		$customer = new Customer($cart->id_customer);

		if (!Validate::isLoadedObject($customer))
		{
			Tools::redirect('index.php?controller=order&step=1');
		}

		$currency = $this->context->currency;
		$total = (float)$cart->getOrderTotal(true, Cart::BOTH);

		// Confirmaci¨®n de metodo de sandbox
		$data_sandbox = Configuration::get('mts_payu_sandbox_mode');	
			
		if ($data_sandbox == 'true')
		{
			$data_login = 'pRRXKOl8ikMmt9u';
			$data_key = '4Vj8eK4rloUd272L48hsrarnUA';
			$data_merchantId = '512321';
			$data_accountId = '000000';
			$data_language = 'es';
			$data_country = 'CO';
			$api_url = 'https://sandbox.api.payulatam.com/payments-api/4.0/service.cgi ';
		}
		else
		{
			$data_login = Configuration::get('mts_payu_api_login');
			$data_key = Configuration::get('mts_payu_api_key');
			$data_merchantId = intval(Configuration::get('mts_payu_api_idmerchant'));
			$data_accountId = intval(Configuration::get('mts_payu_api_idaccount'));
			$data_language = Configuration::get('mts_payu_api_language');
			$data_country = Configuration::get('mts_payu_api_country');
			$api_url = 'https://api.payulatam.com/payments-api/4.0/service.cgi';
		}

		$this->module->validateOrder($cart->id, Configuration::get('PS_OS_MTS_PAYU_PENDING_EFECTY'), $total, $this->module->displayName, NULL, NULL, (int)$currency->id, false, $customer->secure_key);
		
		//Otras variables para la API
		$temp_array = (array)$this->module;	
		// $order_reference = 'payment_test_000010003';
		$order_reference = $this->module->currentOrderReference;
		$id_current_order = $this->module->currentOrder;
		$signature_string = $data_key.'~'. $data_merchantId .'~'. $order_reference .'~'. (int)$total .'~'. $currency->iso_code;
		$payu_signature = md5($signature_string);
		$deviceSessionId = $_POST['deviceSessionId'];
		$cookie = new Cookie('ps');
		$browser_agent = $_SERVER['HTTP_USER_AGENT'];

		$ip_address = '';
		if (getenv('HTTP_CLIENT_IP'))
		{
		$ip_address = getenv('HTTP_CLIENT_IP');
		}
		else if(getenv('HTTP_X_FORWARDED_FOR'))
		{
		$ip_address = getenv('HTTP_X_FORWARDED_FOR');
		}
		else if(getenv('HTTP_X_FORWARDED'))
		{
		$ip_address = getenv('HTTP_X_FORWARDED');
		}
		else if(getenv('HTTP_FORWARDED_FOR'))
		{
		$ip_address = getenv('HTTP_FORWARDED_FOR');
		}
		else if(getenv('HTTP_FORWARDED'))
		{
		$ip_address = getenv('HTTP_FORWARDED');
		}
		else if(getenv('REMOTE_ADDR'))
		{
		$ip_address = getenv('REMOTE_ADDR');
		}
		else
		{
		$ip_address = 'UNKNOWN';
		}

		$temp_stringSplit = str_split(date(DATE_ISO8601, (time()+(7*24*60*60))),19);
		$expirationDate = $temp_stringSplit[0];

		$url = $_SERVER['REQUEST_URI']; //returns the current URL
		$parts = explode('/',$url);
		$notifyUrl = _PS_BASE_URL_SSL_;
		for ($i = 0; $i < count($parts) - 1; $i++) {
			$notifyUrl .= $parts[$i] . "/";
		}
		$notifyUrl .= 'notification'; 

		$jsonSubmitTransaction = [
			"language" => $data_language,
			"command" => "SUBMIT_TRANSACTION",
			"merchant" => [
				"apiKey" => $data_key,
				"apiLogin" => $data_login
			],
			"transaction" => [
				"order" => [
					"accountId" => $data_accountId,
					"referenceCode" => $order_reference,
					"description" => "Pago de la orden # " . $order_reference,
					"language" => $data_language,
					"signature" => $payu_signature,
					"notifyUrl" => $notifyUrl,
					"additionalValues" => [
						"TX_VALUE" => [
							"value" => $total,
							"currency" => $currency->iso_code
						]
					],
					"buyer" => [
						"emailAddress" => $this->context->customer->email,
					]
				],
				"type" => "AUTHORIZATION_AND_CAPTURE",
				"paymentMethod" => strtoupper($_POST['choiced_method']),
				"expirationDate" => $expirationDate,
				"paymentCountry" => $customer_country_info[0]['iso_code'],
				"ipAddress" => $ip_address
			],
			"test" => false
		];

		$jsonApiQuery= json_encode($jsonSubmitTransaction);
	
		$ch = curl_init($api_url);
		curl_setopt_array($ch, array(
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => $jsonApiQuery,
			CURLOPT_RETURNTRANSFER => TRUE,
			CURLOPT_HTTPHEADER => array(
			'Content-Type: application/json; charset=utf-8',
			'Accept: application/json')
		));

		// JSON Format
		$jsonApiRequest = curl_exec($ch);

		//PHP Array Format
		$result = json_decode($jsonApiRequest , true);

		$paymentUntil = str_split($result['transactionResponse']['extraParameters']['EXPIRATION_DATE'],10);
		$paymentReference = $result['transactionResponse']['extraParameters']['REFERENCE'];
		$urlDownloadPDF = $result['transactionResponse']['extraParameters']['URL_PAYMENT_RECEIPT_PDF'];
		$urlViewHTML = $result['transactionResponse']['extraParameters']['URL_PAYMENT_RECEIPT_HTML'];

		$paymentUntilDay = date('d/m/Y', $paymentUntil[0]);
		$paymentUntilHour = date('G:i:s', $paymentUntil[0]);

		session_start();

		$_SESSION['mtspayuapi']['query'] = $jsonSubmitTransaction;
		$_SESSION['mtspayuapi']['result'] = $result;
		$_SESSION['mtspayuapi']['additionalData']['id_order'] = $id_current_order;

		if (($result['code'] == 'SUCCESS' && $result['transactionResponse']['state'] != 'PENDING') || $result['code'] == 'ERROR' )
		{
			$objOrder = new Order($this->module->currentOrder);
			$objOrder->setCurrentState(Configuration::get('PS_OS_CANCELED'));
			Tools::redirect('index.php?fc=module&module='.$this->module->name.'&controller=efecty_declined');
		}			
		else
		{
			$objOrder = new Order($this->module->currentOrder);
			$objOrder->setCurrentState(Configuration::get('PS_OS_MTS_PAYU_PENDING_EFECTY'));
			Tools::redirect('index.php?fc=module&module='.$this->module->name.'&controller=efecty_pending');
		}
	}
}
