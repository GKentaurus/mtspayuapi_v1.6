<?php
/**
* 2016 Metasysco
*
* AVISO DE LICENCIA
*
* Este archivo fuente está sujeto a la Academic Free License (AFL 3.0)
* El cual está incluido en el archivo LICENCE.txt.
* También se encuentra disponible en línea, en la siguiente URL:
* http://opensource.org/licenses/afl-3.0.php
* Si por algún motivo usted no recibió una copia de esta licencia,
* o no pudo obtenerlo a través de la URL, por favor envíe un correo a
* info@metasysco.com, y en la brevedad de lo posible se le enviará una
* copia inmediata.
*
* ADVERTENCIA
*
* No edite, modifique o altére el código de este archivo, si usted
* está tiene planeado a futuro actualizar la plataforma Prestashop 
* a una nueva versión (Aplicable para la versión de prestashop 1.6.x.x).
* Si usted desea modificar este módulo para su necesidad, por favor
* contactenos por medio del correo electrónico development@metasysco.com
* o visite nuestra página web http://www.metasysco.com para mas información.
*
* @author Carlos Moreno <carlos.moreno@metasysco.com.co>
* @copyright 2016 Metasysco S.A.S.
* @version 1.1.0
* @license http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0) 
*/

class MtsPayuApiEfecty_DeclinedModuleFrontController extends ModuleFrontController 
{
	public function initContent()
	{
		include_once(_PS_MODULE_DIR_.'../config/config.inc.php');
		include_once(_PS_MODULE_DIR_.'../config/settings.inc.php');
		parent::initContent();

		session_start();

		if ($_SESSION['mtspayuapi']['result']['code'] == 'SUCCESS')
		{
			// Opción seleccionada para el pago (card - pse - efecty - baloto)
			$this->context->smarty->assign('paymentMethod', 'efecty');

			// Estado de la transaccion (approved - pending - declined)
			$this->context->smarty->assign('paymentState', 'declined');

			// ID de la orden (en caso de querer reintentar compra)
			$this->context->smarty->assign('id_order', $_SESSION['mtspayuapi']['additionalData']['id_order']);

			// Verificar que la API haya recibido bien la información

			// Opción seleccionada para el pago (success - error)
			$this->context->smarty->assign('resultQuery', 'success');

			// Nombre de la empresa
			if (Configuration::get('mts_payu_company_name') != '' && 
				Configuration::get('mts_payu_company_name') != null)
			{
				$this->context->smarty->assign('companyName', Configuration::get('mts_payu_company_name'));
			}

			// NIT de la empresa
			if (Configuration::get('mts_payu_company_nit') != '' && 
				Configuration::get('mts_payu_company_nit') != null)
			{
				$this->context->smarty->assign('companyNit', Configuration::get('mts_payu_company_nit'));
			}

			// Codigo de orden de Prestashop
			if (isset($_SESSION['mtspayuapi']['query']['transaction']['order']['referenceCode']) && 
				!empty($_SESSION['mtspayuapi']['query']['transaction']['order']['referenceCode']))
			{
				$this->context->smarty->assign('referenceOrder', $_SESSION['mtspayuapi']['query']['transaction']['order']['referenceCode']);
			}

			// Valor a Pagar
			if (isset($_SESSION['mtspayuapi']['query']['transaction']['order']['additionalValues']['TX_VALUE']['value']) && 
				!empty($_SESSION['mtspayuapi']['query']['transaction']['order']['additionalValues']['TX_VALUE']['value']))
			{
				$this->context->smarty->assign('value', $_SESSION['mtspayuapi']['query']['transaction']['order']['additionalValues']['TX_VALUE']['value']);
			}

			// Moneda
			if (isset($_SESSION['mtspayuapi']['query']['transaction']['order']['additionalValues']['TX_VALUE']['currency']) && 
				!empty($_SESSION['mtspayuapi']['query']['transaction']['order']['additionalValues']['TX_VALUE']['currency']))
			{
				$this->context->smarty->assign('currency', $_SESSION['mtspayuapi']['query']['transaction']['order']['additionalValues']['TX_VALUE']['currency']);
			}

			// ID de la transaccion de PayU
			if (isset($_SESSION['mtspayuapi']['result']['transactionResponse']['transactionId']) && 
				!empty($_SESSION['mtspayuapi']['result']['transactionResponse']['transactionId']))
			{
				$this->context->smarty->assign('transactionId', $_SESSION['mtspayuapi']['result']['transactionResponse']['transactionId']);
			}

			// ID de la transaccion de PayU
			if (isset($_SESSION['mtspayuapi']['result']['transactionResponse']['trazabilityCode']) && 
				!empty($_SESSION['mtspayuapi']['result']['transactionResponse']['trazabilityCode']))
			{
				$this->context->smarty->assign('trazabilityCode', $_SESSION['mtspayuapi']['result']['transactionResponse']['trazabilityCode']);
			}

			// Fecha de expiración
			if (isset($_SESSION['mtspayuapi']['result']['transactionResponse']['extraParameters']['EXPIRATION_DATE']) && 
				!empty($_SESSION['mtspayuapi']['result']['transactionResponse']['extraParameters']['EXPIRATION_DATE']))
			{
				$paymentUntil = str_split($_SESSION['mtspayuapi']['result']['transactionResponse']['extraParameters']['EXPIRATION_DATE'], 10);

				$paymentUntilDay = (string)date('d/m/Y', $paymentUntil[0]);
				$paymentUntilHour = (string)date('G:i:s', $paymentUntil[0]);

				$this->context->smarty->assign('payUntilDay', $paymentUntilDay);
				$this->context->smarty->assign('payUntilHour', $paymentUntilHour);
			}

			// Referencua de pago
			if (isset($_SESSION['mtspayuapi']['result']['transactionResponse']['extraParameters']['REFERENCE']) && 
				!empty($_SESSION['mtspayuapi']['result']['transactionResponse']['extraParameters']['REFERENCE']))
			{
				$this->context->smarty->assign('paymentReference', $_SESSION['mtspayuapi']['result']['transactionResponse']['extraParameters']['REFERENCE']);
			}

			// Link de descarga de comprobante
			if (isset($_SESSION['mtspayuapi']['result']['transactionResponse']['extraParameters']['URL_PAYMENT_RECEIPT_PDF']) && 
				!empty($_SESSION['mtspayuapi']['result']['transactionResponse']['extraParameters']['URL_PAYMENT_RECEIPT_PDF']))
			{
				$this->context->smarty->assign('urlPaymentPDF', $_SESSION['mtspayuapi']['result']['transactionResponse']['extraParameters']['URL_PAYMENT_RECEIPT_PDF']);
			}

			// Link de vista de comprobante
			if (isset($_SESSION['mtspayuapi']['result']['transactionResponse']['extraParameters']['URL_PAYMENT_RECEIPT_HTML']) && 
				!empty($_SESSION['mtspayuapi']['result']['transactionResponse']['extraParameters']['URL_PAYMENT_RECEIPT_HTML']))
			{
				$this->context->smarty->assign('urlPaymentHTML', $_SESSION['mtspayuapi']['result']['transactionResponse']['extraParameters']['URL_PAYMENT_RECEIPT_HTML']);
			}

			// Código de Respuesta
			if (isset($_SESSION['mtspayuapi']['result']['transactionResponse']['responseCode']) && 
				!empty($_SESSION['mtspayuapi']['result']['transactionResponse']['responseCode']))
			{
				$responseCode = $_SESSION['mtspayuapi']['result']['transactionResponse']['responseCode'];

				$data_language = Configuration::get('mts_payu_api_language');

				$sql = 'SELECT `language_'. $data_language .'` FROM `'._DB_PREFIX_.'mts_payu_rc` WHERE `response_code` = "'.$responseCode.'" ORDER BY `response_code` DESC LIMIT 1';

				$errorMessage = Db::getInstance()->executeS($sql);

				$this->context->smarty->assign('errorMessage', $errorMessage[0]["language_{$data_language}"]);
			}

			// ID de la orden en Prestashop (para reintentar compra)
			if (isset($_SESSION['mtspayuapi']['additionalData']['id_order']) && 
				!empty($_SESSION['mtspayuapi']['additionalData']['id_order']))
			{
				$this->context->smarty->assign('id_order', $_SESSION['mtspayuapi']['additionalData']['id_order']);
			}
		}
		elseif ($_SESSION['mtspayuapi']['result']['code'] == 'ERROR')
		{
			// Opción seleccionada para el pago (success - error)
			$this->context->smarty->assign('resultQuery', 'error');
		}

		// ID de la orden en Prestashop (para reintentar compra)
		if (isset($_SESSION['mtspayuapi']['additionalData']['id_order']) && 
			!empty($_SESSION['mtspayuapi']['additionalData']['id_order']))
		{
			$this->context->smarty->assign('id_order', $_SESSION['mtspayuapi']['additionalData']['id_order']);
		}

		// Enlace de la plantilla
		$this->setTemplate('payment_return/efecty.tpl');
	}
}