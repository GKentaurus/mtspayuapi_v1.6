<?php
/**
* 2016 Metasysco
*
* AVISO DE LICENCIA
*
* Este archivo fuente está sujeto a la Academic Free License (AFL 3.0)
* El cual está incluido en el archivo LICENCE.txt.
* También se encuentra disponible en línea, en la siguiente URL:
* http://opensource.org/licenses/afl-3.0.php
* Si por algún motivo usted no recibió una copia de esta licencia,
* o no pudo obtenerlo a través de la URL, por favor envíe un correo a
* info@metasysco.com, y en la brevedad de lo posible se le enviará una
* copia inmediata.
*
* ADVERTENCIA
*
* No edite, modifique o altére el código de este archivo, si usted
* está tiene planeado a futuro actualizar la plataforma Prestashop 
* a una nueva versión (Aplicable para la versión de prestashop 1.6.x.x).
* Si usted desea modificar este módulo para su necesidad, por favor
* contactenos por medio del correo electrónico development@metasysco.com
* o visite nuestra página web http://www.metasysco.com para mas información.
*
* @author Carlos Moreno <carlos.moreno@metasysco.com.co>
* @copyright 2016 Metasysco S.A.S.
* @version 1.1.0
* @license http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0) 
*/
class MtsPayuApiPSE_ValidateModuleFrontController extends ModuleFrontController
{
	public function postProcess()
	{
	
		include_once(_PS_MODULE_DIR_.'../config/config.inc.php');
		include_once(_PS_MODULE_DIR_.'../config/settings.inc.php');
		include_once(_PS_MODULE_DIR_.'../classes/Cookie.php');
		include_once(_PS_MODULE_DIR_.'../classes/order/OrderHistory.php');
		include_once(_PS_MODULE_DIR_.'../classes/order/Order.php');
		parent::initContent();

		$cart = $this->context->cart;

		if ($cart->id_customer == 0 || $cart->id_address_delivery == 0 || $cart->id_address_invoice == 0 || !$this->module->active || empty($cart->getProducts()))
		{
			$array = ['redirect' => 'index.php?controller=order&step=1'];
			$arrayJson = json_encode($array);
			print_r($arrayJson);
			die();
		}

		// print_r(json_encode($_POST));
		// die();

		// Datos de PSE	
		$pse_bank = filter_var($_POST['pse_bank'], FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES); 	//String
		$pse_name = filter_var($_POST['pse_name'], FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);	//String
		$pse_type = filter_var($_POST['pse_type'], FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);	//String
		$pse_dnitype = filter_var($_POST['pse_dnitype'], FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);	//String
		$pse_dni = filter_var($_POST['pse_dni'], FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);	//String
		$pse_phone = filter_var($_POST['pse_phone'], FILTER_SANITIZE_STRING, FILTER_FLAG_NO_ENCODE_QUOTES);	//String

		$pse_errors = [];

		if ($pse_bank == false || $pse_bank == 'invalid' || $pse_bank == '')
		{
			$pse_errors['pse_bank'] = 'Failed';
		}
		else
		{
			$pse_errors['pse_bank'] = 'Ok';
		}

		if ($pse_name == false || $pse_name == '')
		{
			$pse_errors['pse_name'] = 'Failed';
		}
		else
		{
			$pse_errors['pse_name'] = 'Ok';
		}

		if ($pse_dnitype == false || $pse_dnitype == 'invalid' || $pse_dnitype == '')
		{
			$pse_errors['pse_dnitype'] = 'Failed';
		}
		else
		{
			$pse_errors['pse_dnitype'] = 'Ok';
		}

		if ($pse_dni == false || $pse_dni == '')
		{
			$pse_errors['pse_dni'] = 'Failed';
		}
		else
		{
			$pse_errors['pse_dni'] = 'Ok';
		}

		if ($pse_type == false || $pse_type == 'invalid' || $pse_type == '')
		{
			$pse_errors['pse_type'] = 'Failed';
		}
		else
		{
			$pse_errors['pse_type'] = 'Ok';
		}

		if ($pse_phone == false || $pse_phone == '')
		{
			$pse_errors['pse_phone'] = 'Failed';
		}
		else
		{
			$pse_errors['pse_phone'] = 'Ok';
		}

		if (array_search('Failed', $pse_errors)) {
			$json = json_encode($pse_errors);
			print_r($json);
			die();
		}
		else
		{
			die();
		}
	}
}
