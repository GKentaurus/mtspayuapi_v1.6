<?php
/**
* 2016 Metasysco
*
* AVISO DE LICENCIA
*
* Este archivo fuente está sujeto a la Academic Free License (AFL 3.0)
* El cual está incluido en el archivo LICENCE.txt.
* También se encuentra disponible en línea, en la siguiente URL:
* http://opensource.org/licenses/afl-3.0.php
* Si por algún motivo usted no recibió una copia de esta licencia,
* o no pudo obtenerlo a través de la URL, por favor envíe un correo a
* info@metasysco.com, y en la brevedad de lo posible se le enviará una
* copia inmediata.
*
* ADVERTENCIA
*
* No edite, modifique o altére el código de este archivo, si usted
* está tiene planeado a futuro actualizar la plataforma Prestashop 
* a una nueva versión (Aplicable para la versión de prestashop 1.6.x.x).
* Si usted desea modificar este módulo para su necesidad, por favor
* contactenos por medio del correo electrónico development@metasysco.com
* o visite nuestra página web http://www.metasysco.com para mas información.
*
* @author Carlos Moreno <carlos.moreno@metasysco.com.co>
* @copyright 2016 Metasysco S.A.S.
* @version 1.1.0
* @license http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0) 
*/

class MtsPayuApiCard_PendingModuleFrontController extends ModuleFrontController 
{
	public function initContent()
	{
		include_once(_PS_MODULE_DIR_.'../config/config.inc.php');
		include_once(_PS_MODULE_DIR_.'../config/settings.inc.php');
		parent::initContent();

		session_start();

		// Opción seleccionada para el pago (card - pse - efecty - baloto)
		$this->context->smarty->assign('paymentMethod', 'card');

		// Estado de la transaccion (approved - prending - declined)
		$this->context->smarty->assign('paymentState', 'prending');

		// Opción seleccionada para el pago (success - error)
		$this->context->smarty->assign('resultQuery', 'success');

		// Nombre de la empresa
		if (Configuration::get('mts_payu_company_name') != '' && 
			Configuration::get('mts_payu_company_name') != null)
		{
			$this->context->smarty->assign('companyName', Configuration::get('mts_payu_company_name'));
		}

		// NIT de la empresa
		if (Configuration::get('mts_payu_company_nit') != '' && 
			Configuration::get('mts_payu_company_nit') != null)
		{
			$this->context->smarty->assign('companyNit', Configuration::get('mts_payu_company_nit'));
		}

		// Codigo de Orden de Prestashop
		if (isset($_SESSION['mtspayuapi']['query']['transaction']['order']['referenceCode']) && 
			!empty(($_SESSION['mtspayuapi']['query']['transaction']['order']['referenceCode'])))
		{
			$this->context->smarty->assign('referenceOrder', $_SESSION['mtspayuapi']['query']['transaction']['order']['referenceCode']);
		}

		// ID de la orden de PayU
		if (isset($_SESSION['mtspayuapi']['result']['transactionResponse']['orderId']) && 
			!empty($_SESSION['mtspayuapi']['result']['transactionResponse']['orderId']))
		{
			$this->context->smarty->assign('orderID', $_SESSION['mtspayuapi']['result']['transactionResponse']['orderId']);
		}

		// ID de la transacción
		if (isset($_SESSION['mtspayuapi']['result']['transactionResponse']['transactionId']) && 
			!empty($_SESSION['mtspayuapi']['result']['transactionResponse']['transactionId']))
		{
			$this->context->smarty->assign('transactionId', $_SESSION['mtspayuapi']['result']['transactionResponse']['transactionId']);
		}

		// Respuesta de la red de pagos
		if (isset($_SESSION['mtspayuapi']['result']['transactionResponse']['paymentNetworkResponseErrorMessage']) &&
			!empty($_SESSION['mtspayuapi']['result']['transactionResponse']['paymentNetworkResponseErrorMessage']))
		{
			$this->context->smarty->assign('paymentNetworkResponse', $_SESSION['mtspayuapi']['result']['transactionResponse']['paymentNetworkResponseErrorMessage']);
		}

		// Codigo de Trazabilidad
		if (isset($_SESSION['mtspayuapi']['result']['transactionResponse']['trazabilityCode']) && 
			!empty($_SESSION['mtspayuapi']['result']['transactionResponse']['trazabilityCode']))
		{
			$this->context->smarty->assign('trazabilityCode', $_SESSION['mtspayuapi']['result']['transactionResponse']['trazabilityCode']);
		}

		// Codigo de Autorización
		if (isset($_SESSION['mtspayuapi']['result']['transactionResponse']['authorizationCode']) && 
			!empty($_SESSION['mtspayuapi']['result']['transactionResponse']['authorizationCode']))
		{
			$this->context->smarty->assign('authorizationCode', $_SESSION['mtspayuapi']['result']['transactionResponse']['authorizationCode']);
		}

		// Motivo de pago pendiente
		if (isset($_SESSION['result']['transactionResponse']['pendingReason']) && 
			!empty($_SESSION['result']['transactionResponse']['pendingReason']))
		{
			$this->context->smarty->assign('pendingReason', $_SESSION['result']['transactionResponse']['pendingReason']);
		}			

		// Código de Respuesta
		if (isset($_SESSION['mtspayuapi']['result']['transactionResponse']['responseCode']) && 
			!empty($_SESSION['mtspayuapi']['result']['transactionResponse']['responseCode']))
		{
			$responseCode = $_SESSION['mtspayuapi']['result']['transactionResponse']['responseCode'];

			$data_language = Configuration::get('mts_payu_api_language');

			$sql = 'SELECT `language_'. $data_language .'` FROM `'._DB_PREFIX_.'mts_payu_rc` WHERE `response_code` = "'.$responseCode.'" ORDER BY `response_code` DESC LIMIT 1';

			$errorMessage = Db::getInstance()->executeS($sql);

			$this->context->smarty->assign('errorMessage', $errorMessage[0]["language_{$data_language}"]);
		}

		// Fecha de la transacción
		if (isset($_SESSION['mtspayuapi']['result']['transactionResponse']['transactionDate']) && 
			!empty($_SESSION['mtspayuapi']['result']['transactionResponse']['transactionDate']))
		{
			$this->context->smarty->assign('transactionDate', $_SESSION['mtspayuapi']['result']['transactionResponse']['transactionDate']);
		}

		// Valor de la transacción
		if (isset($_SESSION['mtspayuapi']['query']['transaction']['order']['additionalValues']['TX_VALUE']['value']) && 
			!empty($_SESSION['mtspayuapi']['query']['transaction']['order']['additionalValues']['TX_VALUE']['value']))
		{
			$this->context->smarty->assign('value', $_SESSION['mtspayuapi']['query']['transaction']['order']['additionalValues']['TX_VALUE']['value']);
		}

		// Moneda de la transacción
		if (isset($_SESSION['mtspayuapi']['query']['transaction']['order']['additionalValues']['TX_VALUE']['currency']) && 
			!empty($_SESSION['mtspayuapi']['query']['transaction']['order']['additionalValues']['TX_VALUE']['currency']))
		{
			$this->context->smarty->assign('currency', $_SESSION['mtspayuapi']['query']['transaction']['order']['additionalValues']['TX_VALUE']['currency']);
		}			

		// ID de la orden en Prestashop (para reintentar compra)
		if (isset($_SESSION['mtspayuapi']['additionalData']['id_order']) && 
			!empty($_SESSION['mtspayuapi']['additionalData']['id_order']))
		{
			$this->context->smarty->assign('id_order', $_SESSION['mtspayuapi']['additionalData']['id_order']);
		}

		// Enlace de la plantilla
		$this->setTemplate('payment_return/card.tpl');
	}
}