<?php
/**
* 2016 Metasysco
*
* AVISO DE LICENCIA
*
* Este archivo fuente está sujeto a la Academic Free License (AFL 3.0)
* El cual está incluido en el archivo LICENCE.txt.
* También se encuentra disponible en línea, en la siguiente URL:
* http://opensource.org/licenses/afl-3.0.php
* Si por algún motivo usted no recibió una copia de esta licencia,
* o no pudo obtenerlo a través de la URL, por favor envíe un correo a
* info@metasysco.com, y en la brevedad de lo posible se le enviará una
* copia inmediata.
*
* ADVERTENCIA
*
* No edite, modifique o altére el código de este archivo, si usted
* está tiene planeado a futuro actualizar la plataforma Prestashop 
* a una nueva versión (Aplicable para la versión de prestashop 1.6.x.x).
* Si usted desea modificar este módulo para su necesidad, por favor
* contactenos por medio del correo electrónico development@metasysco.com
* o visite nuestra página web http://www.metasysco.com para mas información.
*
* @author Carlos Moreno <carlos.moreno@metasysco.com.co>
* @copyright 2016 Metasysco S.A.S.
* @version 1.1.0
* @license http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0) 
*/

class MtsPayuApiEfecty_OptionModuleFrontController extends ModuleFrontController
{
	public function postProcess()
	{
		include_once(_PS_MODULE_DIR_.'../config/config.inc.php');
		include_once(_PS_MODULE_DIR_.'../config/settings.inc.php');
		include_once(_PS_MODULE_DIR_.'../classes/Cookie.php');
		include_once(_PS_MODULE_DIR_.'../classes/order/OrderHistory.php');
		include_once(_PS_MODULE_DIR_.'../classes/order/Order.php');
		parent::initContent();

		$cart = $this->context->cart;

		if ($cart->id_customer == 0 || $cart->id_address_delivery == 0 || $cart->id_address_invoice == 0 || !$this->module->active || empty($cart->getProducts()))
		{
			Tools::redirect('index.php?controller=order&step=1');
		}

		$authorized = false;
		foreach (Module::getPaymentModules() as $module)
			if ($module['name'] == 'mtspayuapi')
			{
				$authorized = true;
				break;
			}

		if (!$authorized)
			die($this->module->l('Este método de pago no está disponible.', 'mtspayuapi'));

		$customer = new Customer($cart->id_customer);

		if (!Validate::isLoadedObject($customer))
		{
			Tools::redirect('index.php?controller=order&step=1');
		}

		$deviceSessionId = md5(session_id().microtime());		
		$currency = $this->context->currency;
		$total = (float)$cart->getOrderTotal(true, Cart::BOTH);
		$products = $cart->getProducts();

		$shortListProducts = [];

		foreach ($products as $key => $product) {
			if ($key < 6) {
				array_push($shortListProducts, $product['name']);
			}
			else
			{	
				array_push($shortListProducts, 'y más...');
				break;
			}				
		}
		
		// Confirmaci¨®n de metodo de sandbox
		$data_sandbox = Configuration::get('mts_payu_sandbox_mode');	
			
		if ($data_sandbox == 'true')
		{
			$data_company_name = '(nombre de la compañia)';
		}
		else
		{
			$data_company_name = Configuration::get('mts_payu_company_name');
		}
		
		$this->context->smarty->assign('companyName', $data_company_name);
		$this->context->smarty->assign('choiced_method', 'efecty');	
		$this->context->smarty->assign('products', $shortListProducts);
		$this->context->smarty->assign('total', $total);
		$this->context->smarty->assign('currency', $currency->iso_code);
		$this->setTemplate('payment_efectybaloto.tpl');
	}
}