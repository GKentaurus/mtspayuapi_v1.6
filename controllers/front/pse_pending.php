<?php
/**
* 2016 Metasysco
*
* AVISO DE LICENCIA
*
* Este archivo fuente está sujeto a la Academic Free License (AFL 3.0)
* El cual está incluido en el archivo LICENCE.txt.
* También se encuentra disponible en línea, en la siguiente URL:
* http://opensource.org/licenses/afl-3.0.php
* Si por algún motivo usted no recibió una copia de esta licencia,
* o no pudo obtenerlo a través de la URL, por favor envíe un correo a
* info@metasysco.com, y en la brevedad de lo posible se le enviará una
* copia inmediata.
*
* ADVERTENCIA
*
* No edite, modifique o altére el código de este archivo, si usted
* está tiene planeado a futuro actualizar la plataforma Prestashop 
* a una nueva versión (Aplicable para la versión de prestashop 1.6.x.x).
* Si usted desea modificar este módulo para su necesidad, por favor
* contactenos por medio del correo electrónico development@metasysco.com
* o visite nuestra página web http://www.metasysco.com para mas información.
*
* @author Carlos Moreno <carlos.moreno@metasysco.com.co>
* @copyright 2016 Metasysco S.A.S.
* @version 1.1.0
* @license http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0) 
*/

class MtsPayuApiPSE_PendingModuleFrontController extends ModuleFrontController 
{
	public function initContent()
	{
		include_once(_PS_MODULE_DIR_.'../config/config.inc.php');
		include_once(_PS_MODULE_DIR_.'../config/settings.inc.php');
		parent::initContent();

		session_start();

		// Opción seleccionada para el pago (card - pse - efecty - baloto)
		$this->context->smarty->assign('paymentMethod', 'pse');

		// Estado de la transaccion (approved - pending - declined)
		$this->context->smarty->assign('paymentState', 'pending');

		// ID de la orden (en caso de querer reintentar compra)
		$this->context->smarty->assign('id_order', $_SESSION['additionalData']['id_order']);

		// Verificar que la API haya recibido bien la información

		// Opción seleccionada para el pago (success - error)
		$this->context->smarty->assign('resultQuery', 'success');

		// Nombre de la empresa
		if (Configuration::get('mts_payu_company_name') != '' && 
			Configuration::get('mts_payu_company_name') != null)
		{
			$this->context->smarty->assign('companyName', Configuration::get('mts_payu_company_name'));
		}

		// NIT de la empresa
		if (Configuration::get('mts_payu_company_nit') != '' && 
			Configuration::get('mts_payu_company_nit') != null)
		{
			$this->context->smarty->assign('companyNit', Configuration::get('mts_payu_company_nit'));
		}

		// Respuesta de la red de pagos
		if (isset($_SESSION['mtspayuapi']['get']['message']) &&
			!empty($_SESSION['mtspayuapi']['get']['message']))
		{
			$this->context->smarty->assign('paymentNetworkResponse', $_SESSION['mtspayuapi']['get']['message']);
		}

		// Codigo de Orden de Prestashop
		if (isset($_SESSION['mtspayuapi']['get']['referenceCode']) && 
			!empty($_SESSION['mtspayuapi']['get']['referenceCode']))
		{
			$this->context->smarty->assign('referenceOrder', $_SESSION['mtspayuapi']['get']['referenceCode']);
		}

		// ID de la orden de PayU
		if (isset($_SESSION['mtspayuapi']['get']['reference_pol']) && 
			!empty($_SESSION['mtspayuapi']['get']['reference_pol']))
		{
			$this->context->smarty->assign('orderID', $_SESSION['mtspayuapi']['get']['reference_pol']);
		}

		// ID de la transacción
		if (isset($_SESSION['mtspayuapi']['get']['transactionId']) && 
			!empty($_SESSION['mtspayuapi']['get']['transactionId']))
		{
			$this->context->smarty->assign('transactionId', $_SESSION['mtspayuapi']['get']['transactionId']);
		}

		// Codigo de Trazabilidad
		if (isset($_SESSION['mtspayuapi']['get']['trazabilityCode']) && 
			!empty($_SESSION['mtspayuapi']['get']['trazabilityCode']))
		{
			$this->context->smarty->assign('trazabilityCode', $_SESSION['mtspayuapi']['get']['trazabilityCode']);
		}

		// Número de la transacción
		if (isset($_SESSION['mtspayuapi']['get']['cus']) && 
			!empty($_SESSION['mtspayuapi']['get']['cus']))
		{
			$this->context->smarty->assign('cusNumber', $_SESSION['mtspayuapi']['get']['cus']);
		}

		// Código de Respuesta
		if (isset($_SESSION['mtspayuapi']['get']['lapResponseCode']) && 
			!empty($_SESSION['mtspayuapi']['get']['lapResponseCode']))
		{
			$responseCode = $_SESSION['mtspayuapi']['get']['lapResponseCode'];

			$data_language = Configuration::get('mts_payu_api_language');

			$sql = 'SELECT `language_'. $data_language .'` FROM `'._DB_PREFIX_.'mts_payu_rc` WHERE `response_code` = "'.$responseCode.'" ORDER BY `response_code` DESC LIMIT 1';

			$errorMessage = Db::getInstance()->executeS($sql);

			$this->context->smarty->assign('errorMessage', $errorMessage[0]["language_{$data_language}"]);
		}

		// Combinacion de códigos de respuesta
		if (isset($_SESSION['mtspayuapi']['get']['polTransactionState']) && 
			!empty($_SESSION['mtspayuapi']['get']['polTransactionState']) &&
			isset($_SESSION['mtspayuapi']['get']['polResponseCode']) && 
			!empty($_SESSION['mtspayuapi']['get']['polResponseCode']))
		{
			if ($_SESSION['mtspayuapi']['get']['polTransactionState'] == 4 && $_SESSION['get']['polTransactionState'] == 1)
			{
				$this->context->smarty->assign('transactionState', 'Transacción aprobada');
			}
			elseif ($_SESSION['mtspayuapi']['get']['polTransactionState'] == 6 && $_SESSION['get']['polTransactionState'] == 5)
			{
				$this->context->smarty->assign('transactionState', 'Transacción fallida');
			}
			elseif ($_SESSION['mtspayuapi']['get']['polTransactionState'] == 6 && $_SESSION['get']['polTransactionState'] == 4)
			{
				$this->context->smarty->assign('transactionState', 'Transacción rechazada');
			}
			elseif ($_SESSION['mtspayuapi']['get']['polTransactionState'] == 12 && $_SESSION['get']['polTransactionState'] == 9994)
			{
				$this->context->smarty->assign('transactionState', 'Transacción pendiente, por favor revisar si el débito fue realizado en el banco.');
			}
			else{
				$this->context->smarty->assign('transactionState', 'Error en la transacción.');
			}			
		}

		// ID de la orden en Prestashop (para reintentar compra)
		if (isset($_SESSION['mtspayuapi']['additionalData']['id_order']) && 
			!empty($_SESSION['mtspayuapi']['additionalData']['id_order']))
		{
			$this->context->smarty->assign('id_order', $_SESSION['mtspayuapi']['additionalData']['id_order']);
		}
		
		// Fecha de la transacción
		date_default_timezone_set('America/Bogota');
		$date = date('d/m/Y', time());
		$this->context->smarty->assign('transactionDate', $date);

		// Valor de la transacción
		if (isset($_SESSION['mtspayuapi']['get']['TX_VALUE']) && 
			!empty($_SESSION['mtspayuapi']['get']['TX_VALUE']))
		{
			$this->context->smarty->assign('value', $_SESSION['mtspayuapi']['get']['TX_VALUE']);
		}

		// Moneda de la transacción
		if (isset($_SESSION['mtspayuapi']['get']['TX_VALUE']) && 
			!empty($_SESSION['mtspayuapi']['get']['TX_VALUE']))
		{
			$this->context->smarty->assign('currency', $_SESSION['mtspayuapi']['get']['currency']);
		}	

		// Enlace de la plantilla
		$this->setTemplate('payment_return/pse.tpl');
	}
}