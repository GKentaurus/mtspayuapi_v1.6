<?php
/**
* 2016 Metasysco
*
* AVISO DE LICENCIA
*
* Este archivo fuente está sujeto a la Academic Free License (AFL 3.0)
* El cual está incluido en el archivo LICENCE.txt.
* También se encuentra disponible en línea, en la siguiente URL:
* http://opensource.org/licenses/afl-3.0.php
* Si por algún motivo usted no recibió una copia de esta licencia,
* o no pudo obtenerlo a través de la URL, por favor envíe un correo a
* info@metasysco.com, y en la brevedad de lo posible se le enviará una
* copia inmediata.
*
* ADVERTENCIA
*
* No edite, modifique o altére el código de este archivo, si usted
* está tiene planeado a futuro actualizar la plataforma Prestashop 
* a una nueva versión (Aplicable para la versión de prestashop 1.6.x.x).
* Si usted desea modificar este módulo para su necesidad, por favor
* contactenos por medio del correo electrónico development@metasysco.com
* o visite nuestra página web http://www.metasysco.com para mas información.
*
* @author Carlos Moreno <carlos.moreno@metasysco.com.co>
* @copyright 2016 Metasysco S.A.S.
* @version 1.1.0
* @license http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0) 
*/
class MtsPayuApiPSE_GetDataModuleFrontController extends ModuleFrontController
{
	public function postProcess()
	{
	
		include_once(_PS_MODULE_DIR_.'../config/config.inc.php');
		include_once(_PS_MODULE_DIR_.'../config/settings.inc.php');
		include_once(_PS_MODULE_DIR_.'../classes/Cookie.php');
		include_once(_PS_MODULE_DIR_.'../classes/order/OrderHistory.php');
		include_once(_PS_MODULE_DIR_.'../classes/order/Order.php');
		parent::initContent();

		$cart = $this->context->cart;

		session_start();
		$_SESSION['mtspayuapi']['get'] = $_GET;

		if ($_SESSION['mtspayuapi']['get']['lapTransactionState'] == 'APPROVED') 
		{	
			$objOrder = new Order($_SESSION['mtspayuapi']['additionalData']['id_order']);
			$objOrder->setCurrentState(Configuration::get('PS_OS_MTS_PAYU_PAID_PSE'));
			Tools::redirect('index.php?fc=module&module='.$this->module->name.'&controller=pse_approved');
		}
		elseif ($_SESSION['mtspayuapi']['get']['lapTransactionState'] == 'PENDING')
		{
			$objOrder = new Order($_SESSION['mtspayuapi']['additionalData']['id_order']);
			$objOrder->setCurrentState(Configuration::get('PS_OS_MTS_PAYU_PENDING_PSE'));
			Tools::redirect('index.php?fc=module&module='.$this->module->name.'&controller=pse_pending');
		}
		elseif ($_SESSION['mtspayuapi']['get']['lapTransactionState'] == 'DECLINED' || $_SESSION['mtspayuapi']['get']['lapTransactionState'] == 'ERROR' || $_SESSION['mtspayuapi']['get']['lapTransactionState'] == 'EXPIRED')
		{
			$objOrder = new Order($_SESSION['mtspayuapi']['additionalData']['id_order']);
			$objOrder->setCurrentState(Configuration::get('PS_OS_CANCELED'));
			Tools::redirect('index.php?fc=module&module='.$this->module->name.'&controller=pse_declined');
		}

		// echo "<pre>";
		// // print_r($_SESSION['mtspayuapi']['get']);
		// print_r($_SESSION['mtspayuapi']['additionalData']['id_order']);
		// echo "</pre>";
		// die();
	}
}
