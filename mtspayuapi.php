<?php
/**
* 2016 Metasysco
*
* AVISO DE LICENCIA
*
* Este archivo fuente está sujeto a la Academic Free License (AFL 3.0)
* El cual está incluido en el archivo LICENCE.txt.
* También se encuentra disponible en línea, en la siguiente URL:
* http://opensource.org/licenses/afl-3.0.php
* Si por algún motivo usted no recibió una copia de esta licencia,
* o no pudo obtenerlo a través de la URL, por favor envíe un correo a
* info@metasysco.com, y en la brevedad de lo posible se le enviará una
* copia inmediata.
*
* ADVERTENCIA
*
* No edite, modifique o altére el código de este archivo, si usted
* está tiene planeado a futuro actualizar la plataforma Prestashop 
* a una nueva versión (Aplicable para la versión de prestashop 1.6.x.x).
* Si usted desea modificar este módulo para su necesidad, por favor
* contactenos por medio del correo electrónico development@metasysco.com
* o visite nuestra página web http://www.metasysco.com para mas información.
*
* @author Carlos Moreno <carlos.moreno@metasysco.com.co>
* @copyright 2016 Metasysco S.A.S.
* @version 1.1.0
* @license http://opensource.org/licenses/afl-3.0.php  Academic Free License (AFL 3.0) 
*/

if (!defined('_PS_VERSION_'))
	exit;

class MtsPayuApi extends PaymentModule
{
	private $_html = '';
	private $_postErrors = array();

	// Método constructor
	public function __construct()
	{
		$this->name = 'mtspayuapi';
		$this->tab = 'payments_gateways';
		$this->version = '1.2.0';
		$this->author = 'Metasysco S.A.S.';
		$this->author_uri = 'http://www.metasysco.com/';
		$this->author_url = 'http://www.metasysco.com/';
		$this->need_instance = 0;
		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => '1.7');
		$this->bootstrap = true;
		// $this->controllers = array('payment', 'validation');
		$this->is_eu_compatible = 1;
		$this->currencies = true;
		$this->currencies_mode = 'checkbox';
		// $this->limited_countries = array('br', 'ar', 'co', 'mx', 'pa', 'pe');
		
		parent::__construct();

		// Información del módulo
		$this->displayName = $this->l('Metasysco.com - Pagos a través de PayU');
		$this->description = $this->l('Plataforma de pago basada en PayU, en la que su cliente no tendrá; que salir del comercio para realizar el pago. De esta forma podr&aacute; hacer siguimiento de las compras realizadas a través de este método de pago. Solo latinoamérica.');
		
		// Mensaje de confirmación para desinstalar el módulo
		$this->confirmUninstall = $this->l('¿Está seguro que desea desinstalar? Se borrará toda la información relacionada con la API de PayU Latam.');

		// Creación y asignación de variables
		$config = Configuration::getMultiple(
			array(
				'mts_payu_api_key', 
				'mts_payu_api_login', 
				'mts_payu_api_publickey',
				'mts_payu_api_idmerchant', 
				'mts_payu_api_idaccount', 
				'mts_payu_api_country', 
				'mts_payu_api_language', 
				'mts_payu_sandbox_mode', 
				'mts_payu_company_name', 
				'mts_payu_company_nit',
				'mts_payu_extrainfo_card_mode',
				'mts_payu_extrainfo_card_link',
				'mts_payu_extrainfo_pse_mode',
				'mts_payu_extrainfo_pse_link',
				'mts_payu_extrainfo_efecty_mode',
				'mts_payu_extrainfo_efecty_link',
				'mts_payu_extrainfo_baloto_mode',
				'mts_payu_extrainfo_baloto_link'

			)
		);

		if (isset($config['mts_payu_api_key']))
			$this->mts_payu_api_key = $config['mts_payu_api_key'];
			
		if (isset($config['mts_payu_api_login']))
			$this->mts_payu_api_login = $config['mts_payu_api_login'];
			
		if (isset($config['mts_payu_api_publickey']))
			$this->mts_payu_api_publickey = $config['mts_payu_api_publickey'];
			
		if (isset($config['mts_payu_api_idmerchant']))
			$this->mts_payu_api_idmerchant = $config['mts_payu_api_idmerchant'];

		if (isset($config['mts_payu_api_idaccount']))
			$this->mts_payu_api_idaccount = $config['mts_payu_api_idaccount'];

		if (isset($config['mts_payu_api_country']))
			$this->mts_payu_api_country = $config['mts_payu_api_country'];

		if (isset($config['mts_payu_api_language']))
			$this->mts_payu_api_language = $config['mts_payu_api_language'];
			
		if (isset($config['mts_payu_sandbox_mode']))
			$this->mts_payu_sandbox_mode = $config['mts_payu_sandbox_mode'];

		if (isset($config['mts_payu_company_name']))
			$this->mts_payu_company_name = $config['mts_payu_company_name'];

		if (isset($config['mts_payu_company_nit']))
			$this->mts_payu_company_nit = $config['mts_payu_company_nit'];

		if (isset($config['mts_payu_extrainfo_card_mode']))
			$this->mts_payu_extrainfo_card_mode = $config['mts_payu_extrainfo_card_mode'];

		if (isset($config['mts_payu_extrainfo_card_link']))
			$this->mts_payu_extrainfo_card_link = $config['mts_payu_extrainfo_card_link'];

		if (isset($config['mts_payu_extrainfo_pse_mode']))
			$this->mts_payu_extrainfo_pse_mode = $config['mts_payu_extrainfo_pse_mode'];

		if (isset($config['mts_payu_extrainfo_pse_link']))
			$this->mts_payu_extrainfo_pse_link = $config['mts_payu_extrainfo_pse_link'];

		if (isset($config['mts_payu_extrainfo_efecty_mode']))
			$this->mts_payu_extrainfo_efecty_mode = $config['mts_payu_extrainfo_efecty_mode'];

		if (isset($config['mts_payu_extrainfo_efecty_link']))
			$this->mts_payu_extrainfo_efecty_link = $config['mts_payu_extrainfo_efecty_link'];

		if (isset($config['mts_payu_extrainfo_baloto_mode']))
			$this->mts_payu_extrainfo_baloto_mode = $config['mts_payu_extrainfo_baloto_mode'];

		if (isset($config['mts_payu_extrainfo_baloto_link']))
			$this->mts_payu_extrainfo_baloto_link = $config['mts_payu_extrainfo_baloto_link'];

		// Comprobación de variables
		if (
			!isset($this->mts_payu_api_key) || 
			!isset($this->mts_payu_api_login) || 
			!isset($this->mts_payu_api_publickey) || 
			!isset($this->mts_payu_api_idmerchant) || 
			!isset($this->mts_payu_api_idaccount) || 
			!isset($this->mts_payu_api_country) || 
			!isset($this->mts_payu_api_language) || 
			!isset($this->mts_payu_sandbox_mode) || 
			!isset($this->mts_payu_company_name) || 
			!isset($this->mts_payu_company_nit) ||
			!isset($this->mts_payu_extrainfo_card_mode) || 
			!isset($this->mts_payu_extrainfo_card_link) || 
			!isset($this->mts_payu_extrainfo_pse_mode) || 
			!isset($this->mts_payu_extrainfo_pse_link) || 
			!isset($this->mts_payu_extrainfo_efecty_mode) || 
			!isset($this->mts_payu_extrainfo_efecty_link) || 
			!isset($this->mts_payu_extrainfo_baloto_mode) || 
			!isset($this->mts_payu_extrainfo_baloto_link) || 

			empty($this->mts_payu_api_key) || 
			empty($this->mts_payu_api_login) || 
			empty($this->mts_payu_api_publickey) || 
			empty($this->mts_payu_api_idmerchant) || 
			empty($this->mts_payu_api_idaccount) || 
			empty($this->mts_payu_api_country) || 
			empty($this->mts_payu_api_language) ||
			empty($this->mts_payu_sandbox_mode) || 
			empty($this->mts_payu_company_name) || 
			empty($this->mts_payu_company_nit) ||
			empty($this->mts_payu_extrainfo_card_mode) || 
			empty($this->mts_payu_extrainfo_card_link) || 
			empty($this->mts_payu_extrainfo_pse_mode) || 
			empty($this->mts_payu_extrainfo_pse_link) || 
			empty($this->mts_payu_extrainfo_efecty_mode) || 
			empty($this->mts_payu_extrainfo_efecty_link) || 
			empty($this->mts_payu_extrainfo_baloto_mode) || 
			empty($this->mts_payu_extrainfo_baloto_link)	
		)
			$this->warning = $this->l('Los campos API Key, API Login, Id de la Cuenta e Id de Comercio deben ser configurados a través de este módulo.');
		
		if (!count(Currency::checkPaymentCurrencies($this->id)))
			$this->warning = $this->l('Ninguna moneda ha sido configurada para este módulo.');
	}

	public function install()
	{
		if (
			!parent::install() || 
			!self::installDB() ||
			!$this->registerHook('payment') || 
			!$this->registerHook('displayPaymentEU') || 
			!$this->registerHook('paymentReturn') ||
			!$this->registerHook('header')
			)
		{
			return false;
		}

		if (!$this->installOrderState())
		{
			return false;
		}

		return true;
	}

	public function installDB()
	{	
		$noErrors = [];

		Db::getInstance()->execute('
			DROP TABLE IF EXISTS `'._DB_PREFIX_.'mts_payu_rc`
		');

		$noErrors[] = Db::getInstance()->execute('
			CREATE TABLE `'._DB_PREFIX_.'mts_payu_rc`(
				`response_code` varchar(255) NOT NULL,
				`language_en` varchar(255) NOT NULL,
				`language_es` varchar(255) NOT NULL,
				`language_pt` varchar(255) NOT NULL
			)
		');


		$noErrors[] = Db::getInstance()->execute("
			INSERT INTO `"._DB_PREFIX_."mts_payu_rc` (`response_code`, `language_en`, `language_es`, `language_pt`) VALUES
			('ERROR', 'There was a general error.', 'Ocurrió un error general.', 'Ocorreu um erro geral.'),
			('APPROVED', 'The transaction was approved.', 'La transacción fue aprobada.', 'A transação foi aprovada.'),
			('ANTIFRAUD_REJECTED', 'The transaction was rejected by the anti-fraud system.', 'La transacción fue rechazada por el sistema anti-fraude.', 'A transação foi rejeitada pelo sistema anti fraude.'),
			('PAYMENT_NETWORK_REJECTED', 'The financial network rejected the transaction.', 'La red financiera rechazó la transacción.', 'A rede financeira rejeitou a transação.'),
			('ENTITY_DECLINED', 'The transaction was declined by the bank or financial network because of an error.', 'La transacción fue declinada por el banco o por la red financiera debido a un error.', 'A transação foi rejeitada pelo banco ou pela rede financeira devido a um erro.'),
			('INTERNAL_PAYMENT_PROVIDER_ERROR', 'An error occurred in the system trying to process the payment.', 'Ocurrió un error en el sistema intentando procesar el pago.', 'Ocorreu um erro no sistema tentando processar o pagamento.'),
			('INACTIVE_PAYMENT_PROVIDER', 'The payment provider was not active.', 'El proveedor de pagos no se encontraba activo.', 'O fornecedor de pagamentos não estava ativo.'),
			('DIGITAL_CERTIFICATE_NOT_FOUND', 'The financial network reported an authentication error.', 'La red financiera reportó un error en la autenticación.', 'A rede financeira relatou um erro na autenticação.'),
			('INVALID_EXPIRATION_DATE_OR_SECURITY_CODE', 'The security code or expiration date was invalid.', 'El código de seguridad o la fecha de expiración estaba inválido.', 'O código de segurança ou a data de expiração estava inválido.'),
			('INVALID_RESPONSE_PARTIAL_APPROVAL', 'Invalid response type. The entity response is a partial approval and should be automatically canceled by the system.', 'Tipo de respuesta no válida. La entidad aprobó parcialmente la transacción y debe ser cancelada automáticamente por el sistema.', 'Tipo de resposta inválida. A entidade financeira aprovou parcialmente a transação e deve ser cancelado automaticamente pelo sistema.'),
			('INSUFFICIENT_FUNDS', 'The account had insufficient funds.', 'La cuenta no tenía fondos suficientes.', 'A conta não tinha crédito suficiente.'),
			('CREDIT_CARD_NOT_AUTHORIZED_FOR_INTERNET_TRANSACTIONS', 'The credit card was not authorized for internet transactions.', 'La tarjeta de crédito no estaba autorizada para transacciones por Internet.', 'O cartão de crédito não estava autorizado para transações pela Internet.'),
			('INVALID_TRANSACTION', 'The financial network reported that the transaction was invalid.', 'La red financiera reportó que la transacción fue inválida.', 'A rede financeira relatou que a transação foi inválida.'),
			('INVALID_CARD', 'The card is invalid.', 'La tarjeta es inválida.', 'O cartão é inválido.'),
			('EXPIRED_CARD', 'The card has expired.', 'La tarjeta ya expiró.', 'O cartão já expirou.'),
			('RESTRICTED_CARD', 'The card has a restriction.', 'La tarjeta presenta una restricción.', 'O cartão apresenta uma restrição.'),
			('CONTACT_THE_ENTITY', 'You should contact the bank.', 'Debe contactar al banco.', 'Você deve entrar em contato com o banco.'),
			('REPEAT_TRANSACTION', 'You must repeat the transaction.', 'Se debe repetir la transacción.', 'Deve-se repetir a transação.'),
			('ENTITY_MESSAGING_ERROR', 'The financial network reported a communication error with the bank.', 'La red financiera reportó un error de comunicaciones con el banco.', 'A rede financeira relatou um erro de comunicações com o banco.'),
			('BANK_UNREACHABLE', 'The bank was not available.', 'El banco no se encontraba disponible.', 'O banco não se encontrava disponível.'),
			('EXCEEDED_AMOUNT', 'The transaction exceeds the amount set by the bank.', 'La transacción excede un monto establecido por el banco.', 'A transação excede um montante estabelecido pelo banco.'),
			('NOT_ACCEPTED_TRANSACTION', 'The transaction was not accepted by the bank for some reason.', 'La transacción no fue aceptada por el banco por algún motivo.', 'A transação não foi aceita pelo banco por algum motivo.'),
			('ERROR_CONVERTING_TRANSACTION_AMOUNTS', 'An error occurred converting the amounts to the payment currency.', 'Ocurrió un error convirtiendo los montos a la moneda de pago.', 'Ocorreu um erro convertendo os montantes para a moeda de pagamento.'),
			('EXPIRED_TRANSACTION', 'The transaction expired.', 'La transacción expiró.', 'A transação expirou.'),
			('PENDING_TRANSACTION_REVIEW', 'The transaction was stopped and must be revised, this can occur because of security filters.', 'La transacción fue detenida y debe ser revisada, esto puede ocurrir por filtros de seguridad.', 'A transação foi parada e deve ser revista, isto pode ocorrer por filtros de segurança.'),
			('PENDING_TRANSACTION_CONFIRMATION', 'The transaction is subject to confirmation.', 'La transacción está pendiente de ser confirmada.', 'A transação está pendente de confirmação.'),
			('PENDING_TRANSACTION_TRANSMISSION', 'The transaction is subject to be transmitted to the financial network. This usually applies to transactions with cash payment means.', 'La transacción está pendiente para ser trasmitida a la red financiera. Normalmente esto aplica para transacciones con medios de pago en efectivo.', 'A transação está pendente para ser transmitida para a rede financeira. Normalmente isto se aplica para transações com formas de pagamento em dinheiro.'),
			('PAYMENT_NETWORK_BAD_RESPONSE', 'The message returned by the financial network is inconsistent.', 'El mensaje retornado por la red financiera es inconsistente.', 'A mensagem retornada pela rede financeira é inconsistente.'),
			('PAYMENT_NETWORK_NO_CONNECTION', 'Could not connect to the financial network.', 'No se pudo realizar la conexión con la red financiera.', 'Não foi possível realizar a conexão com a rede financeira.'),
			('PAYMENT_NETWORK_NO_RESPONSE', 'Financial Network did not respond.', 'La red financiera no respondió.', 'A rede financeira não respondeu.'),
			('FIX_NOT_REQUIRED', 'Transactions clinic: internal handling code.', 'Clínica de transacciones: Código de manejo interno.', 'Clínica de transações: Código de operação interna.'),
			('AUTOMATICALLY_FIXED_AND_SUCCESS_REVERSAL', 'Transactions clinic: internal handling code.', 'Clínica de transacciones: Código de manejo interno.', 'Clínica de transações: Código de operação interna.'),
			('AUTOMATICALLY_FIXED_AND_UNSUCCESS_REVERSAL', 'Transactions clinic: internal handling code.', 'Clínica de transacciones: Código de manejo interno.', 'Clínica de transações: Código de operação interna.'),
			('AUTOMATIC_FIXED_NOT_SUPPORTED', 'Transactions clinic: internal handling code.', 'Clínica de transacciones: Código de manejo interno.', 'Clínica de transações: Código de operação interna.'),
			('NOT_FIXED_FOR_ERROR_STATE', 'Transactions clinic: internal handling code.', 'Clínica de transacciones: Código de manejo interno.', 'Clínica de transações: Código de operação interna.'),
			('ERROR_FIXING_AND_REVERSING', 'Transactions clinic: internal handling code.', 'Clínica de transacciones: Código de manejo interno.', 'Clínica de transações: Código de operação interna.'),
			('ERROR_FIXING_INCOMPLETE_DATA', 'Transactions clinic: internal handling code.', 'Clínica de transacciones: Código de manejo interno.', 'Clínica de transações: Código de operação interna.');
		");

		
		$result = true;

		foreach ($noErrors as $key => $value)
		{
			if ($value === 0 || $value === false)
			{
				$result = false;
			}
		}

		return $result;
	}

	public function uninstall()
	{
		if (
			!Configuration::deleteByName('mts_payu_api_key') || 
			!Configuration::deleteByName('mts_payu_api_login') || 
			!Configuration::deleteByName('mts_payu_api_publickey') || 
			!Configuration::deleteByName('mts_payu_api_idmerchant') || 
			!Configuration::deleteByName('mts_payu_api_idaccount') || 
			!Configuration::deleteByName('mts_payu_api_country') || 
			!Configuration::deleteByName('mts_payu_api_language') ||
			!Configuration::deleteByName('mts_payu_sandbox_mode') || 
			!Configuration::deleteByName('mts_payu_company_name') || 
			!Configuration::deleteByName('mts_payu_company_nit') || 
			!Configuration::deleteByName('mts_payu_extrainfo_card_mode') || 
			!Configuration::deleteByName('mts_payu_extrainfo_card_link') || 
			!Configuration::deleteByName('mts_payu_extrainfo_pse_mode') || 
			!Configuration::deleteByName('mts_payu_extrainfo_pse_link') || 
			!Configuration::deleteByName('mts_payu_extrainfo_efecty_mode') || 
			!Configuration::deleteByName('mts_payu_extrainfo_efecty_link') || 
			!Configuration::deleteByName('mts_payu_extrainfo_baloto_mode') || 
			!Configuration::deleteByName('mts_payu_extrainfo_baloto_link') || 
			!self::uninstallDB() ||
			!parent::uninstall()
		)
			return false;
		
		// Si el proceso se completo satisfactoriamente, retorna TRUE
		Configuration::updateValue('mts_payu_api_key', null);
		Configuration::updateValue('mts_payu_api_login', null);
		Configuration::updateValue('mts_payu_api_publickey', null);
		Configuration::updateValue('mts_payu_api_idmerchant', null);
		Configuration::updateValue('mts_payu_api_idaccount', null);
		Configuration::updateValue('mts_payu_api_country', null);
		Configuration::updateValue('mts_payu_api_language', null);
		Configuration::updateValue('mts_payu_sandbox_mode', null);
		Configuration::updateValue('mts_payu_company_name', null);
		Configuration::updateValue('mts_payu_company_nit', null);
		Configuration::updateValue('mts_payu_extrainfo_card_mode', null);
		Configuration::updateValue('mts_payu_extrainfo_card_link', null);
		Configuration::updateValue('mts_payu_extrainfo_pse_mode', null);
		Configuration::updateValue('mts_payu_extrainfo_pse_link', null);
		Configuration::updateValue('mts_payu_extrainfo_efecty_mode', null);
		Configuration::updateValue('mts_payu_extrainfo_efecty_link', null);
		Configuration::updateValue('mts_payu_extrainfo_baloto_mode', null);
		Configuration::updateValue('mts_payu_extrainfo_baloto_link', null);
		return true;
	}

	public function uninstallDB()
	{
		Db::getInstance()->execute('
			DROP TABLE IF EXISTS `'._DB_PREFIX_.'mts_payu_rc`
		');

		return true;
	}

	public function installOrderState()
	{
		if (Configuration::get('PS_OS_MTS_PAYU_PENDING_CARD') < 1)
		{
			$order_state = new OrderState();
			$order_state->name = array((int)Configuration::get('PS_LANG_DEFAULT') => pSQL($this->l('Por confirmar: T. Crédito/Débito')));
			$order_state->invoice = false;
			$order_state->send_email = false;
			$order_state->module_name = $this->name;
			$order_state->color = '#8796a5';
			$order_state->unremovable = true;
			$order_state->hidden = false;
			$order_state->logable = false;
			$order_state->delivery = false;
			$order_state->shipped = false;
			$order_state->paid = false;
			
			if ($order_state->add())
			{
				Configuration::updateValue('PS_OS_MTS_PAYU_PENDING_CARD', $order_state->id);

				copy(dirname(__FILE__).'/os_pending.gif',
				dirname(__FILE__).'/../../img/os/'.$order_state->id.'.gif');
				copy(dirname(__FILE__).'/os_pending.gif',
				dirname(__FILE__).'/../../img/tmp/order_state_mini_'.$order_state->id.'.gif');
			}
			else
			{
				return false;
			}
		}

		if (Configuration::get('PS_OS_MTS_PAYU_PENDING_PSE') < 1)
		{
			$order_state = new OrderState();
			$order_state->name = array((int)Configuration::get('PS_LANG_DEFAULT') => pSQL($this->l('Por confirmar: Pagos en Línea (PSE)')));
			$order_state->invoice = false;
			$order_state->send_email = false;
			$order_state->module_name = $this->name;
			$order_state->color = '#9fa587';
			$order_state->unremovable = true;
			$order_state->hidden = false;
			$order_state->logable = false;
			$order_state->delivery = false;
			$order_state->shipped = false;
			$order_state->paid = false;
			
			if ($order_state->add())
			{
				Configuration::updateValue('PS_OS_MTS_PAYU_PENDING_PSE', $order_state->id);

				copy(dirname(__FILE__).'/os_pending.gif',
				dirname(__FILE__).'/../../img/os/'.$order_state->id.'.gif');
				copy(dirname(__FILE__).'/os_pending.gif',
				dirname(__FILE__).'/../../img/tmp/order_state_mini_'.$order_state->id.'.gif');
			}
			else
			{
				return false;
			}
		}

		if (Configuration::get('PS_OS_MTS_PAYU_PENDING_EFECTY') < 1)
		{
			$order_state = new OrderState();
			$order_state->name = array((int)Configuration::get('PS_LANG_DEFAULT') => pSQL($this->l('Por confirmar: Efecty')));
			$order_state->invoice = false;
			$order_state->send_email = false;
			$order_state->module_name = $this->name;
			$order_state->color = '#a5a187';
			$order_state->unremovable = true;
			$order_state->hidden = false;
			$order_state->logable = false;
			$order_state->delivery = false;
			$order_state->shipped = false;
			$order_state->paid = false;
			
			if ($order_state->add())
			{
				Configuration::updateValue('PS_OS_MTS_PAYU_PENDING_EFECTY', $order_state->id);

				copy(dirname(__FILE__).'/os_pending.gif',
				dirname(__FILE__).'/../../img/os/'.$order_state->id.'.gif');
				copy(dirname(__FILE__).'/os_pending.gif',
				dirname(__FILE__).'/../../img/tmp/order_state_mini_'.$order_state->id.'.gif');
			}
			else
			{
				return false;
			}
		}

		if (Configuration::get('PS_OS_MTS_PAYU_PENDING_BALOTO') < 1)
		{
			$order_state = new OrderState();
			$order_state->name = array((int)Configuration::get('PS_LANG_DEFAULT') => pSQL($this->l('Por confirmar: Baloto')));
			$order_state->invoice = false;
			$order_state->send_email = false;
			$order_state->module_name = $this->name;
			$order_state->color = '#87a598';
			$order_state->unremovable = true;
			$order_state->hidden = false;
			$order_state->logable = false;
			$order_state->delivery = false;
			$order_state->shipped = false;
			$order_state->paid = false;
			
			if ($order_state->add())
			{
				Configuration::updateValue('PS_OS_MTS_PAYU_PENDING_BALOTO', $order_state->id);

				copy(dirname(__FILE__).'/os_pending.gif',
				dirname(__FILE__).'/../../img/os/'.$order_state->id.'.gif');
				copy(dirname(__FILE__).'/os_pending.gif',
				dirname(__FILE__).'/../../img/tmp/order_state_mini_'.$order_state->id.'.gif');
			}
			else
			{
				return false;
			}
		}

		if (Configuration::get('PS_OS_MTS_PAYU_PAID_CARD') < 1)
		{
			$order_state = new OrderState();
			$order_state->name = array((int)Configuration::get('PS_LANG_DEFAULT') => pSQL($this->l('Pagado: T. Crédito/Débito')));
			$order_state->invoice = true;
			$order_state->send_email = false;
			$order_state->module_name = $this->name;
			$order_state->color = '#007fff';
			$order_state->unremovable = true;
			$order_state->hidden = false;
			$order_state->logable = true;
			$order_state->delivery = false;
			$order_state->shipped = false;
			$order_state->paid = true;
			$order_state->pdf_invoice = false;
			$order_state->pdf_delivery = false;
			$order_state->deleted = false;
			
			if ($order_state->add())
			{
				Configuration::updateValue('PS_OS_MTS_PAYU_PAID_CARD', $order_state->id);

				copy(dirname(__FILE__).'/os_paid.gif',
				dirname(__FILE__).'/../../img/os/'.$order_state->id.'.gif');
				copy(dirname(__FILE__).'/os_paid.gif',
				dirname(__FILE__).'/../../img/tmp/order_state_mini_'.$order_state->id.'.gif');
			}
			else
			{
				return false;
			}
		}

		if (Configuration::get('PS_OS_MTS_PAYU_PAID_PSE') < 1)
		{
			$order_state = new OrderState();
			$order_state->name = array((int)Configuration::get('PS_LANG_DEFAULT') => pSQL($this->l('Pagado: Pagos en Línea (PSE)')));
			$order_state->invoice = true;
			$order_state->send_email = false;
			$order_state->module_name = $this->name;
			$order_state->color = '#ccff00';
			$order_state->unremovable = true;
			$order_state->hidden = false;
			$order_state->logable = true;
			$order_state->delivery = false;
			$order_state->shipped = false;
			$order_state->paid = true;
			
			if ($order_state->add())
			{
				Configuration::updateValue('PS_OS_MTS_PAYU_PAID_PSE', $order_state->id);

				copy(dirname(__FILE__).'/os_paid.gif',
				dirname(__FILE__).'/../../img/os/'.$order_state->id.'.gif');
				copy(dirname(__FILE__).'/os_paid.gif',
				dirname(__FILE__).'/../../img/tmp/order_state_mini_'.$order_state->id.'.gif');
			}
			else
			{
				return false;
			}
		}

		if (Configuration::get('PS_OS_MTS_PAYU_PAID_EFECTY') < 1)
		{
			$order_state = new OrderState();
			$order_state->name = array((int)Configuration::get('PS_LANG_DEFAULT') => pSQL($this->l('Pagado: Efecty')));
			$order_state->invoice = true;
			$order_state->send_email = false;
			$order_state->module_name = $this->name;
			$order_state->color = '#ffdd00';
			$order_state->unremovable = true;
			$order_state->hidden = false;
			$order_state->logable = true;
			$order_state->delivery = false;
			$order_state->shipped = false;
			$order_state->paid = true;
			
			if ($order_state->add())
			{
				Configuration::updateValue('PS_OS_MTS_PAYU_PAID_EFECTY', $order_state->id);

				copy(dirname(__FILE__).'/os_paid.gif',
				dirname(__FILE__).'/../../img/os/'.$order_state->id.'.gif');
				copy(dirname(__FILE__).'/os_paid.gif',
				dirname(__FILE__).'/../../img/tmp/order_state_mini_'.$order_state->id.'.gif');
			}
			else
			{
				return false;
			}
		}

		if (Configuration::get('PS_OS_MTS_PAYU_PAID_BALOTO') < 1)
		{
			$order_state = new OrderState();
			$order_state->name = array((int)Configuration::get('PS_LANG_DEFAULT') => pSQL($this->l('Pagado: Baloto')));
			$order_state->invoice = true;
			$order_state->send_email = false;
			$order_state->module_name = $this->name;
			$order_state->color = '#00ff90';
			$order_state->unremovable = true;
			$order_state->hidden = false;
			$order_state->logable = true;
			$order_state->delivery = false;
			$order_state->shipped = false;
			$order_state->paid = true;
			
			if ($order_state->add())
			{
				Configuration::updateValue('PS_OS_MTS_PAYU_PAID_BALOTO', $order_state->id);

				copy(dirname(__FILE__).'/os_paid.gif',
				dirname(__FILE__).'/../../img/os/'.$order_state->id.'.gif');
				copy(dirname(__FILE__).'/os_paid.gif',
				dirname(__FILE__).'/../../img/tmp/order_state_mini_'.$order_state->id.'.gif');
			}
			else
			{
				return false;
			}
		}

		return true;
	}

	private function _postValidation()
	{
		if (Tools::isSubmit('btn_API_Data_Submit'))
		{
			if (!Tools::getValue('mts_payu_api_key'))
			{
				$this->_postErrors[] = $this->l('La API Key es requerida.');
			}

			if (!Tools::getValue('mts_payu_api_login'))
			{
				$this->_postErrors[] = $this->l('El API Login es requerido.');
			}

			if (!Tools::getValue('mts_payu_api_idmerchant'))
			{
				$this->_postErrors[] = $this->l('El Id del Comercio es requerido.');
			}

			if (!Tools::getValue('mts_payu_api_idaccount'))
			{
				$this->_postErrors[] = $this->l('El Id de la Cuenta es requerido.');
			}

			if (!Tools::getValue('mts_payu_api_country') || Tools::getValue('mts_payu_api_country') == 'invalid' )
			{
				$this->_postErrors[] = $this->l('Seleccione un país válido.');
			}

			if (!Tools::getValue('mts_payu_api_language') || Tools::getValue('mts_payu_api_language') == 'invalid' )
			{
				$this->_postErrors[] = $this->l('Seleccione un idioma válido.');
			}
		}

		if (Tools::isSubmit('btn_Company_Data_Submit'))
		{
			if (!Tools::getValue('mts_payu_company_name'))
			{
				$this->_postErrors[] = $this->l('El nombre o razón social de la compañia es requerida.');
			}

			if (!Tools::getValue('mts_payu_company_nit'))
			{
				$this->_postErrors[] = $this->l('El NIT o identificación de la compañia es requerido.');
			}
		}

		if (Tools::isSubmit('btn_ExtraInfo_Data_Submit'))
		{
			if (Tools::getValue('mts_payu_extrainfo_card_mode') == true && !Tools::getValue('mts_payu_extrainfo_card_link'))
			{
				$this->_postErrors[] = $this->l('Ingrese el enlace con la información adicional para Tarjetas Crédito/Débito');
			}

			if (Tools::getValue('mts_payu_extrainfo_pse_mode') == true && !Tools::getValue('mts_payu_extrainfo_pse_link'))
			{
				$this->_postErrors[] = $this->l('Ingrese el enlace con la información adicional para Pagos en Línea (PSE)');
			}

			if (Tools::getValue('mts_payu_extrainfo_efecty_mode') == true && !Tools::getValue('mts_payu_extrainfo_efecty_link'))
			{
				$this->_postErrors[] = $this->l('Ingrese el enlace con la información adicional para Pagos en efectívo (Efecty)');
			}

			if (Tools::getValue('mts_payu_extrainfo_baloto_mode') == true && !Tools::getValue('mts_payu_extrainfo_baloto_link'))
			{
				$this->_postErrors[] = $this->l('Ingrese el enlace con la información adicional para Pagos en efectívo (Baloto)');
			}
		}
	}

	private function _postProcess()
	{
		if (Tools::isSubmit('btn_API_Data_Submit'))
		{
			Configuration::updateValue('mts_payu_api_key', Tools::getValue('mts_payu_api_key'));
			Configuration::updateValue('mts_payu_api_login', Tools::getValue('mts_payu_api_login'));
			Configuration::updateValue('mts_payu_api_publickey', Tools::getValue('mts_payu_api_publickey'));
			Configuration::updateValue('mts_payu_api_idmerchant', Tools::getValue('mts_payu_api_idmerchant'));
			Configuration::updateValue('mts_payu_api_idaccount', Tools::getValue('mts_payu_api_idaccount'));
			Configuration::updateValue('mts_payu_api_country', Tools::getValue('mts_payu_api_country'));
			Configuration::updateValue('mts_payu_api_language', Tools::getValue('mts_payu_api_language'));
			$this->_html .= $this->displayConfirmation($this->l('Configuración de la API actualizada'));
		}
		
		if (Tools::isSubmit('btn_Company_Data_Submit'))
		{
			Configuration::updateValue('mts_payu_sandbox_mode', Tools::getValue('mts_payu_sandbox_mode'));
			Configuration::updateValue('mts_payu_company_name', Tools::getValue('mts_payu_company_name'));
			Configuration::updateValue('mts_payu_company_nit', Tools::getValue('mts_payu_company_nit'));
			$this->_html .= $this->displayConfirmation($this->l('Información del cliente actualizada'));
		}

		if (Tools::isSubmit('btn_ExtraInfo_Data_Submit'))
		{
			Configuration::updateValue('mts_payu_extrainfo_card_mode', Tools::getValue('mts_payu_extrainfo_card_mode'));
			Configuration::updateValue('mts_payu_extrainfo_card_link', Tools::getValue('mts_payu_extrainfo_card_link'));
			Configuration::updateValue('mts_payu_extrainfo_pse_mode', Tools::getValue('mts_payu_extrainfo_pse_mode'));
			Configuration::updateValue('mts_payu_extrainfo_pse_link', Tools::getValue('mts_payu_extrainfo_pse_link'));
			Configuration::updateValue('mts_payu_extrainfo_efecty_mode', Tools::getValue('mts_payu_extrainfo_efecty_mode'));
			Configuration::updateValue('mts_payu_extrainfo_efecty_link', Tools::getValue('mts_payu_extrainfo_efecty_link'));
			Configuration::updateValue('mts_payu_extrainfo_baloto_mode', Tools::getValue('mts_payu_extrainfo_baloto_mode'));
			Configuration::updateValue('mts_payu_extrainfo_baloto_link', Tools::getValue('mts_payu_extrainfo_baloto_link'));
			$this->_html .= $this->displayConfirmation($this->l('Información adicional en los pagos actualizada'));
		}
	}

	private function _displayMPA()
	{
		return $this->display(__FILE__, 'infos.tpl');
	}

	public function getContent()
	{
		$this->_html = '';

		if (Tools::isSubmit('btn_API_Data_Submit'))
		{
			$this->_postValidation();
			if (!count($this->_postErrors))
				$this->_postProcess();
			else
				foreach ($this->_postErrors as $err)
					$this->_html .= $this->displayError($err);
		}

		if (Tools::isSubmit('btn_Company_Data_Submit'))
		{
			$this->_postValidation();
			if (!count($this->_postErrors))
				$this->_postProcess();
			else
				foreach ($this->_postErrors as $err)
					$this->_html .= $this->displayError($err);
		}

		if (Tools::isSubmit('btn_ExtraInfo_Data_Submit'))
		{
			$this->_postValidation();
			if (!count($this->_postErrors))
				$this->_postProcess();
			else
				foreach ($this->_postErrors as $err)
					$this->_html .= $this->displayError($err);
		}

		$this->_html .= $this->_displayMPA();
		$this->_html .= $this->renderForm();

		return $this->_html;
	}

	public function hookHeader($params)
	{
		$this->context->controller->addCSS($this->_path.'views/templates/css/mts_payu_style.css', 'all');
		$this->context->controller->addJS($this->_path.'views/templates/js/mts_payu_script.js', 'all');
	}

	public function hookPayment($params)
	{
		if (!$this->active)
			return;
		if (!$this->checkCurrency($params['cart']))
			return;
		$this->smarty->assign(array(
			'this_path' => $this->_path,
			'this_path_mtspayuapi' => $this->_path,
			'this_path_ssl' => Tools::getShopDomainSsl(true, true).__PS_BASE_URI__.'modules/'.$this->name.'/'
		));

		$this->context->controller->addCSS($this->_path.'views/templates/css/mts_payu_style.css', 'all');
		$this->context->controller->addJS($this->_path.'views/templates/js/mts_payu_script.js', 'all');
		
		return $this->display(__FILE__, 'payment_options.tpl');
	}

	public function hookDisplayPaymentEU($params)
	{
		if (!$this->active)
			return;
		if (!$this->checkCurrency($params['cart']))
			return;

		$payment_options = array(
			'cta_text' => $this->l('Pago a través PayU'),
			'logo' => Media::getMediaPath(_PS_MODULE_DIR_.$this->name.'/logo.png'),
			'action' => $this->context->link->getModuleLink($this->name, 'validation', array(), true)
		);

		return $payment_options;
	}

	public function hookPaymentReturn($params)
	{
		if (!$this->active)
			return;

		$state = $params['objOrder']->getCurrentState();
		if (in_array(
			$state, array(
				Configuration::get('PS_OS_MTS_PAYU_PENDING_CARD'), 
				Configuration::get('PS_OS_MTS_PAYU_PAID_CARD'), 
				Configuration::get('PS_OS_MTS_PAYU_PENDING_PSE'), 
				Configuration::get('PS_OS_MTS_PAYU_PAID_PSE'), 
				Configuration::get('PS_OS_MTS_PAYU_PENDING_EFECTY'),
				Configuration::get('PS_OS_MTS_PAYU_PAID_EFECTY'),
				Configuration::get('PS_OS_MTS_PAYU_PENDING_BALOTO'),
				Configuration::get('PS_OS_MTS_PAYU_PAID_BALOTO')
		)))
		{
			$this->smarty->assign(array(
				'total_to_pay' => Tools::displayPrice($params['total_to_pay'], $params['currencyObj'], false),
				'status' => 'ok',
				'id_order' => $params['objOrder']->id
			));
			if (isset($params['objOrder']->reference) && !empty($params['objOrder']->reference))
				$this->smarty->assign('reference', $params['objOrder']->reference);
		}
		else
			$this->smarty->assign('status', 'failed');

		return $this->display(__FILE__, 'payment_return.tpl');
	}

	public function checkCurrency($cart)
	{
		$currency_order = new Currency((int)($cart->id_currency));
		$currencies_module = $this->getCurrency((int)$cart->id_currency);

		if (is_array($currencies_module))
			foreach ($currencies_module as $currency_module)
				if ($currency_order->id == $currency_module['id_currency'])
					return true;
		return false;
	}

	public function renderForm()
	{
		$optionCountries['query'] = [
			0 => [
				'id_country' => 'invalid',
				'name' => '--'
			],
			1 => [
				'id_country' => 'AR',
				'name' => 'Argentina'
			],
			2 => [
				'id_country' => 'BR',
				'name' => 'Brasil'
			],
			3 => [
				'id_country' => 'CO',
				'name' => 'Colombia'
			],
			4 => [
				'id_country' => 'MX',
				'name' => 'México'
			],
			5 => [
				'id_country' => 'PA',
				'name' => 'Panamá'
			],
			6 => [
				'id_country' => 'PE',
				'name' => 'Perú'
			]
		];

		$optionCountries['id'] = 'id_country';
		$optionCountries['name'] = 'name';

		$optionLanguages['query'] = [
			0 => [
				'id_language' => 'invalid',
				'name' => '--'
			],
			1 => [
				'id_language' => 'es',
				'name' => 'Español'
			],
			2 => [
				'id_language' => 'en',
				'name' => 'Inglés'
			],
			3 => [
				'id_language' => 'pt',
				'name' => 'Portugués'
			]
		];

		$optionLanguages['id'] = 'id_language';
		$optionLanguages['name'] = 'name';

		$company_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Datos de la Compañia'),
					'icon' => 'icon-envelope'
				),
				'input' => array(
					array(
						'type' => 'switch',
						'label' => $this->l('En pruebas'),
						'desc' => $this->l('Si habilita, la API funcionará en modo de pruebas'),
						'name' => 'mts_payu_sandbox_mode',
						'required' => false,
						'is_bool' => true,
						'values' => array(
							array(
								'id' => 'active_on',
								'value' => 1,
								'label' => $this->l('Si')
							),
							array(
								'id' => 'active_off',
								'value' => 0,
								'label' => $this->l('No')
							),
						),

					),
					array(
						'type' => 'text',
						'label' => $this->l('Razón social'),
						'desc' => $this->l('Ingrese la razón social o el nombre de la compañía'),
						'name' => 'mts_payu_company_name',
						'required' => false
					),
					array(
						'type' => 'text',
						'label' => $this->l('NIT'),
						'desc' => $this->l('Ingrese el NIT o identificación de la compañía'),
						'name' => 'mts_payu_company_nit',
						'required' => true
					)
				),
				'submit' => array(
					'title' => $this->l('Guardar'),
					'name' => 'btn_Company_Data_Submit'
				)
			),
		);

		$api_data_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Datos de la API de PayU'),
					'icon' => 'icon-envelope'
				),
				'input' => array(
					array(
						'type' => 'text',
						'label' => $this->l('API Key'),
						'desc' => $this->l('Ingrese el API Key que le fue proporcionado por PayU'),
						'name' => 'mts_payu_api_key',
						'required' => true
					),
					array(
						'type' => 'text',
						'label' => $this->l('API Login'),
						'desc' => $this->l('Ingrese el API Login que le fue proporcionado por PayU'),
						'name' => 'mts_payu_api_login',
						'required' => true
					),
					array(
						'type' => 'text',
						'label' => $this->l('Llave pública'),
						'desc' => $this->l('Ingrese la Llave Pública que le fue proporcionado por PayU'),
						'name' => 'mts_payu_api_publickey',
						'required' => false
					),
					array(
						'type' => 'text',
						'label' => $this->l('ID del comercio'),
						'desc' => $this->l('Ingrese el ID de Comercio que le fue proporcionado por PayU'),
						'name' => 'mts_payu_api_idmerchant',
						'required' => true
					),
					array(
						'type' => 'text',
						'label' => $this->l('ID de la cuenta'),
						'desc' => $this->l('Ingrese el ID de la Cuenta que le fue proporcionado por PayU'),
						'name' => 'mts_payu_api_idaccount',
						'required' => true
					),
					array(
						'type' => 'select',
						'label' => $this->l('País'),
						'desc' => $this->l('Seleccione el país registrado para esta cuenta de PayU.'),
						'name' => 'mts_payu_api_country',
						'required' => true,
						'options' => $optionCountries
					),
					array(
						'type' => 'select',
						'label' => $this->l('Idioma'),
						'desc' => $this->l('Seleccione el idioma que desea utilizar para la API.'),
						'name' => 'mts_payu_api_language',
						'required' => true,
						'options' => $optionLanguages
					)
				),
				'submit' => array(
					'title' => $this->l('Guardar'),
					'name' => 'btn_API_Data_Submit'
				)
			),
		);

		$info_extra_form = array(
			'form' => array(
				'legend' => array(
					'title' => $this->l('Información adicional en los métodos de págo'),
					'icon' => 'icon-envelope'
				),
				'input' => array(
					array(
						'type' => 'switch',
						'label' => $this->l('Mostrar en Tarjetas Crédito/Débito'),
						'desc' => $this->l('Si se desahabilita, no se mostrará el enlace en este método de pago'),
						'name' => 'mts_payu_extrainfo_card_mode',
						'required' => false,
						'is_bool' => true,
						'values' => array(
							array(
								'id' => 'active_on',
								'value' => 1,
								'label' => $this->l('Si')
							),
							array(
								'id' => 'active_off',
								'value' => 0,
								'label' => $this->l('No')
							),
						),

					),
					array(
						'type' => 'text',
						'label' => $this->l('Enlace para Tarjetas Crédito/Débito'),
						'desc' => $this->l('Ingrese el enlace a la información adicional para Tarjetas Crédito/Débito'),
						'name' => 'mts_payu_extrainfo_card_link',
						'required' => false
					),
					array(
						'type' => 'switch',
						'label' => $this->l('Mostrar en Pagos en Línea (PSE)'),
						'desc' => $this->l('Si se desahabilita, no se mostrará el enlace en este método de pago'),
						'name' => 'mts_payu_extrainfo_pse_mode',
						'required' => false,
						'is_bool' => true,
						'values' => array(
							array(
								'id' => 'active_on',
								'value' => 1,
								'label' => $this->l('Si')
							),
							array(
								'id' => 'active_off',
								'value' => 0,
								'label' => $this->l('No')
							),
						),

					),
					array(
						'type' => 'text',
						'label' => $this->l('Enlace para Pagos en Línea (PSE)'),
						'desc' => $this->l('Ingrese el enlace a la información adicional para Pagos en Línea (PSE)'),
						'name' => 'mts_payu_extrainfo_pse_link',
						'required' => false
					),
					array(
						'type' => 'switch',
						'label' => $this->l('Mostrar en Efecty'),
						'desc' => $this->l('Si se desahabilita, no se mostrará el enlace en este método de pago'),
						'name' => 'mts_payu_extrainfo_efecty_mode',
						'required' => false,
						'is_bool' => true,
						'values' => array(
							array(
								'id' => 'active_on',
								'value' => 1,
								'label' => $this->l('Si')
							),
							array(
								'id' => 'active_off',
								'value' => 0,
								'label' => $this->l('No')
							),
						),

					),
					array(
						'type' => 'text',
						'label' => $this->l('Enlace para Efecty'),
						'desc' => $this->l('Ingrese el enlace a la información adicional para Efecty'),
						'name' => 'mts_payu_extrainfo_efecty_link',
						'required' => false
					),
					array(
						'type' => 'switch',
						'label' => $this->l('Mostrar en Baloto'),
						'desc' => $this->l('Si se desahabilita, no se mostrará el enlace en este método de pago'),
						'name' => 'mts_payu_extrainfo_baloto_mode',
						'required' => false,
						'is_bool' => true,
						'values' => array(
							array(
								'id' => 'active_on',
								'value' => 1,
								'label' => $this->l('Si')
							),
							array(
								'id' => 'active_off',
								'value' => 0,
								'label' => $this->l('No')
							),
						),

					),
					array(
						'type' => 'text',
						'label' => $this->l('Enlace para Baloto'),
						'desc' => $this->l('Ingrese el enlace a la información adicional para Baloto'),
						'name' => 'mts_payu_extrainfo_baloto_link',
						'required' => false
					)
				),
				'submit' => array(
					'title' => $this->l('Guardar'),
					'name' => 'btn_ExtraInfo_Data_Submit'
				)
			),
		);

		$helper = new HelperForm();
		$helper->show_toolbar = false;
		$helper->table = $this->table;
		$lang = new Language((int)Configuration::get('PS_LANG_DEFAULT'));
		$helper->default_form_language = $lang->id;
		$helper->allow_employee_form_lang = Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') ? Configuration::get('PS_BO_ALLOW_EMPLOYEE_FORM_LANG') : 0;
		$this->fields_form = array();
		$helper->id = (int)Tools::getValue('id_carrier');
		$helper->identifier = $this->identifier;
		$helper->submit_action = array('btn_Company_Data_Submit', 'btn_API_Data_Submit', 'btn_ExtraInfo_Data_Submit');
		$helper->currentIndex = $this->context->link->getAdminLink('AdminModules', false).'&configure='.$this->name.'&tab_module='.$this->tab.'&module_name='.$this->name;
		$helper->token = Tools::getAdminTokenLite('AdminModules');
		$helper->tpl_vars = array(
			'fields_value' => $this->getConfigFieldsValues(),
			'languages' => $this->context->controller->getLanguages(),
			'id_language' => $this->context->language->id
		);

		return $helper->generateForm(array($company_form, $api_data_form, $info_extra_form));
	}

	public function getConfigFieldsValues()
	{
		return array(
			'mts_payu_api_key' => Tools::getValue('mts_payu_api_key', Configuration::get('mts_payu_api_key')),
			'mts_payu_api_login' => Tools::getValue('mts_payu_api_login', Configuration::get('mts_payu_api_login')),
			'mts_payu_api_publickey' => Tools::getValue('mts_payu_api_publickey', Configuration::get('mts_payu_api_publickey')),
			'mts_payu_api_idmerchant' => Tools::getValue('mts_payu_api_idmerchant', Configuration::get('mts_payu_api_idmerchant')),
			'mts_payu_api_idaccount' => Tools::getValue('mts_payu_api_idaccount', Configuration::get('mts_payu_api_idaccount')),
			'mts_payu_api_country' => Tools::getValue('mts_payu_api_country', Configuration::get('mts_payu_api_country')),
			'mts_payu_api_language' => Tools::getValue('mts_payu_api_language', Configuration::get('mts_payu_api_language')),
			'mts_payu_sandbox_mode' => Tools::getValue('mts_payu_sandbox_mode', Configuration::get('mts_payu_sandbox_mode')),
			'mts_payu_company_name' => Tools::getValue('mts_payu_company_name', Configuration::get('mts_payu_company_name')),
			'mts_payu_company_nit' => Tools::getValue('mts_payu_company_nit', Configuration::get('mts_payu_company_nit')),
			'mts_payu_extrainfo_card_mode' => Tools::getValue('mts_payu_extrainfo_card_mode', Configuration::get('mts_payu_extrainfo_card_mode')),
			'mts_payu_extrainfo_card_link' => Tools::getValue('mts_payu_extrainfo_card_link', Configuration::get('mts_payu_extrainfo_card_link')),
			'mts_payu_extrainfo_pse_mode' => Tools::getValue('mts_payu_extrainfo_pse_mode', Configuration::get('mts_payu_extrainfo_pse_mode')),
			'mts_payu_extrainfo_pse_link' => Tools::getValue('mts_payu_extrainfo_pse_link', Configuration::get('mts_payu_extrainfo_pse_link')),
			'mts_payu_extrainfo_efecty_mode' => Tools::getValue('mts_payu_extrainfo_efecty_mode', Configuration::get('mts_payu_extrainfo_efecty_mode')),
			'mts_payu_extrainfo_efecty_link' => Tools::getValue('mts_payu_extrainfo_efecty_link', Configuration::get('mts_payu_extrainfo_efecty_link')),
			'mts_payu_extrainfo_baloto_mode' => Tools::getValue('mts_payu_extrainfo_baloto_mode', Configuration::get('mts_payu_extrainfo_baloto_mode')),
			'mts_payu_extrainfo_baloto_link' => Tools::getValue('mts_payu_extrainfo_baloto_link', Configuration::get('mts_payu_extrainfo_baloto_link'))
		);
	}
	
	public function smartyGetCacheId($name = null)
	{
		return $this->getCacheId($name);
	}
	
	public function smartyClearCache($template, $cache_id =null, $compile_id =null)
	{
		return $this->_clearCache($template, $cache_id, $compile_id);
	}
}